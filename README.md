# *Eucalyptus* Genome

Repository for scripts related to the *Eucalyptus* genome analysis. Make note of the bash script headers as some jobs were run on different cluster systems.

[Spreadsheet](https://docs.google.com/spreadsheets/d/1kgCXL3GB2o9xHkLu86ccIHauoIPRVLDWrz_ak5pV75w/edit?usp=sharing)


## **Project background**

Current breeding strategies for eucalypt tree improvement are less accurate than haplotype-based molecular breeding strategies being deployed in other crop species. However, to use haplotype-based molecular breeding strategies for tree improvement, a high-quality reference genome is required for the species. The current *E. grandis* genome has been assembled using short-read technologies, which pose a variety of challenges and result in fragmented and incomplete reference genomes. Towards this, long-read sequencing technologies offer a solution to many of these challenges, and can be used to improve / assemble high quality reference genomes. In addition, long-read sequencing has been used in combination with short-read sequencing, to obtain high quality reference genomes for two parental species by sequencing an interspecific hybrid. This reduces the cost of generating such assemblies, as fewer sequence information is required than when these genomes are assembled independently.

As such, this project aims to assemble the genome of an F1 *E. urophylla* x *E. grandis* hybrid, to assemble high-quality reference genomes for both *E. urophylla* and *E. grandis*.

In order to obtain the best possible genome, multiple genome assembly programs will be tested as well as a trio-binning approach for haplotype assembly of *E. grandis* and *E. urophylla* as discussed in a paper by Koren *et al* 2018.


## **1) DNA Sequencing**

DNA was extracted from leaf tissue of and F1 *E. urophylla* x *E. grandis* hybrid. Sequencing of the hybrid
was performed using the ONT MinION and PromethION v9.4 platform.

Raw long-read sequencing data located at:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/PromethION

Additionally, to use the trio-binning approach proposed by Koren *et al*, 151 bp PE Illumina sequencing data was generated for the F1 hybrid, as well as the *E. urophylla* and *E. grandis* parents.

Raw short-read sequencing data can be found at:

*       /projects/EBP/CBC/eucalyptus/rawReads/Illumina

The data generated for both platforms are displayed below, along with the theoretical estimated genome coverage:

**Illumina sequencing data:**

*	*E. urophylla* parent: 127.5 Gb, 196.2x
*	*E. grandis* parent: 141.6 Gb, 217.8x
*	F1 Hybrid: 116.1 Gb, 178.61x

**Nanopore sequencing data:**

*	MinION Run1: 11.18 Gb called, 9.5 Gb passed
*	MinION Run2: 2.55 Gb called, 2.32 Gb passed
*	PromethION: 61.59 Gb called, 56.69 Gb passed
*	Total: 75.32 Gb called, 68.51 Gb passed

Total estimated ONT coverage: 105.4x


## **2) Long-read adapter removal and read QC**

Removal of sequencing adapters was performed using **PoreChop**. All scripts relating to the removal of read adapters from long read sequencing data can be found in the **Long-Read base-calling and QC** folder.

Scripts for base-calling with the **Guppy basecaller** (GPU version) are also available in this directory.

Illumina read QC (**FastQC**) and trimming (**Trimmomatic**) scripts can be found in the **Illumina read trimming and QC** folder.

Genome size was estimated for the F1 hybrid and both parents using GenomeScope and Jellyfish v2.

Results can be viewed for the:

*	[F1 hybrid](http://genomescope.org/analysis.php?code=y6aJStQvFNC3miEcfY1w)
*	[*E. urophylla* parent](http://genomescope.org/analysis.php?code=U0pwJgQ0cxIbaoZRU5XJ)
*	[*E. grandis* parent](http://genomescope.org/analysis.php?code=yJVSLG4asdhIYMWbWEKA)


## **3) Contaminant check**

This step is focused on removing reads that are contaminants (reads that are from organisms other than the species being sequenced). This step makes use of **Kraken2** (for Illumina reads) and **Centrifuge** (for long-reads).

Scripts for Kraken2 analysis and read removal, as well as Centrifuge analysis of raw ONT reads can be found in the **Contaminant check** directory.

**Input (Kraken2):**

*	PE Illumina reads short reads in FASTQ format
*	Kraken classification database
*	**kraken_Illumina.sh** script for generating unclassified and classified read sets

**Output:**

*	Classified and unclassified short read data for each read set (we will want to use the unclassified set going forward)

**Input (Centrifuge):**

*	Raw long reads in FASTQ format
*	Sequence index database of possible contaminants (human, archaea, bacterial and fungal)
*	**Centrifuge_euc.sh** script to generate a classification report

**Output:**

*	Centrifuge classification report

**Note:** Additional python scripts must be used to remove contaminated reads identified in the report. Scripts related to analysis and removal of contaminated reads from trio-binned ONT reads can be found in the **Hybrid_assembly_Canu_binned/Contaminant_QC folder**.

Resulting reports can be found here: [Centifuge (long-read) and Kraken2 (Illumina read) reports](https://docs.google.com/spreadsheets/d/1kgCXL3GB2o9xHkLu86ccIHauoIPRVLDWrz_ak5pV75w/edit?usp=sharing)


## **4) Genome assembly**

Multiple genome assembly programs were tested to obtain the best possible assembly for the F1 hybrid.


### Independant parental and hybrid short read genome assemblies

As a means to trio-binning, both parental genomes were independantly assembled using **Masurca** and 151 bp PE Illumina sequencing data. This also gives a baseline assembly with which to compare Hybrid and Long-read based assemblies.

Assembly scripts can be found in the **Masurca parent genome assembly** directory as well as the **Masurca Genome Assembly** folders.

[Short-read assembly statistics](https://drive.google.com/file/d/1iTPWglWvjcOwhq-c9e_g2sDNjAYNSnGb/view?usp=sharing)

**Input:**

*	Base-called Illumina sequencing reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run 
*	**masurca_ONT.sh** generates the assemble.sh file to run the Masurca assembly using your specified parameters and FASTQ files
 
**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory


### SHASTA genome assembly

SHASTA genome assembler was used to assembly the F1 hybrid genome using uncorrected Nanopore sequencing reads (although I used the entire read set uncluding contaminants, we recommend using an uncontaminated read set).

All assembly scripts can be found in the **Long-read genome assembly/Shasta genome assembly** folder.

Assembly quality assessment results can be seen [here](https://docs.google.com/spreadsheets/d/1aTbXIBGCENBpppWbEmMfj6gf7PoY4OCJ0c3e3TGMfqI/edit?usp=sharing)

**Input:**

*	Base-called PoreChopped Nanopore sequencing reads in FASTA format
*	**shasta.sh** script to run the shasta genoem assembly program with minimum input read length of 500 bp (this parameter may be changed)

**Output:**

*	Draft long-read genome assembly

### FALCON genome assembly

The FALCON genome assembler was used to assemble the F1 hybrid genome using Nanopore sequencing reads. The header within the FASTA file had to be changed for the FALCON program to allow assembly (this script can be found in the Falcon Genome assembly subdirectory.

FALCON also requires an environmental setup before running. All steps are better explained in the relevant directory.

Unfortunately FALCON-Unzip is not compatible with Nanopore data, so PurgeHaplotigs was used to remove redundant contigs and as potential alternative to haplotype phasing (see below).

All assembly scripts (including scripts related to the assembly quality assessment) can be found in the **Long-read genome assembly/Falcon Genome assembly** directory.

[Falcon genome assembly statistics results](https://drive.google.com/file/d/1iJyaJqX9VMLZh9BtXDC0F26bVpj3gOaV/view?usp=sharing)

**Input:**

*	Header modified Nanopore reads in FASTA format
*	**falcon_add.sh** script to activate environment for the python script to run in
*	**fc_run.py** script to specify run parameters (this must be optimised for your individual species)
*	**subreads.fasta.fofn** file specifying the path to the ONT FASTA reads (the modified ones)

**Output:**

*	Fasta assembly file (which likely contains redundant reads)

### Flye genome assembly

The Flye genome assembler was also tested to assemble th F1 hybrid genome using uncorrected (and unfiltered) Nanopore sequencing reads.

All assembly scripts related to the assembly (and assembly quality check) can be found in the **Long-read genome assembly/Flye genome assembly** folder.

Assembly quality results can be found [here](https://docs.google.com/spreadsheets/d/1aTbXIBGCENBpppWbEmMfj6gf7PoY4OCJ0c3e3TGMfqI/edit?usp=sharing)  

**Input:**

*	Raw ONT reads in FASTQ format
*	**flye_raw.sh** script to assemble the ONT reads

**Output:**
	
*	Assembled genome

### Masurca hybrid genome assembly

Masurca was used to assemble the F1 hybrid genome with both Nanopore and Illumina short reads.

All scripts related to the assembly (including assembly quality asseessment) can be found in the **Masurca Genome Assembly** folder.

Results for the hybrid assembly (using short and long reads) for the F1 hybrid are attached below.

[Masurca hybrid genome assembly statistics](https://docs.google.com/spreadsheets/d/1Z1wfsx1yg7BcoNfln5DZ7vanzRXRWm0CB3Cz1lim50M/edit?usp=sharing)

**Input:**
	
*	Raw ONT long reads in FASTQ or FASTA format
*	PE Illumina (hybrid) short reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh fiel to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory

### Masurca assembly of Canu trio-binned reads

Phased genome assembly was tested using binned and uncontaminated ONT reads. Reads were binned with parental short-reads using Trio-Canu. The binned reads were assembled independantly using the Masurca hybrid genome assembler (using parental short reads and binned long-reads of the F1 hybrid. Two separate assemblies were run, one per bin).

**Input:**

*	Uncontaminated and binned ONT long reads in FASTA format
*	Unclassified PE Illumina parental short reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh fiel to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory


## **5) Genome alignments to reference**

### Genome assembly comparisons (using minimap alignments via D-GENIES)

Mimimap2 alignments were performed for the following combinations to check if alignments do work as a possible alternative method for binning.

D-GENIES is an interactive website where these alignments can be performed using only the FASTA assembly file, and creates dotplots of the alignments (using either minimap2 or mashmap, which is chosen by the user). The resulting alignments can be downloaded and stored by the user to look at at a later time. The following comparisons were made and the alignment files were downloaded and are available [here](https://drive.google.com/drive/folders/1dCep6VYzy530tFMWPwCWXKY6KcCqZ7eK?usp=sharing).

*	Masurca hybrid assembly of 10Mb and longer contigs to *E. grandis* reference
*	Masurca hybrid long contigs to *E. grandis* reference
*	Masurca short read *E. urophylla* to reference *E. grandis*
*	Masurca short read *E. grandis* to reference *E. grandis*
*	Masurca short read *E. urophylla* to short read *E. grandis*
*	Masurca short read *E. urophylla* to Masurca hybrid aseembly
*	Masurca short read *E. grandis* to Masurca hybrid assembly
*	Masurca hybrid assembly to reference + chloroplast + mitochondrial
*	Masurca purged hybrid assembly to reference + chloroplast + mitochondrial
*	Falcon assembly to reference + chloroplast + mitochondrial

Alternatively one can use minimap and then using **nucmer** from the **Mummer** package one can create dotplot comparisons (either with RStudio or with nucmer itself, example scripts can be found in the **Genome_Assembly_Alignments** folder).


## **6) Genome assembly quality**

All genome assemblies were evaluated for quality using **BUSCO** to identify the number of core genes present in the assembled genome, and **QUAST** to check genome assembly statistics such as N50, # contigs etc.

All scripts related to these for each assembly can be found in their respective directories, but follow a simple layout:

*	BUSCO: run_BUSCO.py -i /path/to/assembly.fasta -l /path/to/embryophyte/data -o /name_of_output_folder -m geno 

*	QUAST: quast.py /path/to/assembly.fasta -o Name_of_output_folder

Links are provided to spreadsheet containing the results in the respective assembly discussions above.


## **7) Genome assembly improvements**

This sectiotion is focused on improving the draft genome to obtain reference quality genomes (not annotation).


### **1) Removal of redundant reads**

Redundant reads were removed from the the following assemblies using **Purge Haplotigs**

#### FALCON hybrid Genome assembly

Statistics of the assembly after removal of redundant reads using purge haplotigs are also shown in the spreadsheet above.

[Purge haplotigs histogram](https://drive.google.com/file/d/1A5a8zDZCcGQWuX6ID1Eb3YLwOpX_bz0-/view?usp=sharing)


#### Masurca hybrid genome assembly

Statistics of the assembly after removal of redundant reads using purge haplotigs are also shown in the spreadsheet above.

[Purge hapotigs histogram](https://drive.google.com/file/d/1i_rEbqorwNEvgWkn1DAwcUbEFS32DnNb/view?usp=sharing)


### **2) Masking of repetitive elements**

Although I gave a numbered order, this step is separate from removal of redundant reads and can be performed in tandem.

This step is recommended before genome annotation or identification of structural variants but comes after scaffolding.

Repetitive elements were masked for the following genome assemblies:

*    Masurca hybrid genome assembly
*    *E. grandis* haplogenome
*    *E. urophylla* haplogenome

After repetitive elements have been masked one can obtain an estimate of the genome repetitiveness, as well as the type of elements involved in repetitiveness.

This will be included for the final genome you want to use and publish, as part of the genome annotation process.

The masking process is better explained in the **Masurca hybrid assembly repeat analysis** directory.


### **3) Checking genome size**

To check if the assembled genome size is correct (and that the reduced assembly size compared to the estimated size is not due to reads not being assembled), **bwa mem** can be used along with the **flagstat** function from **samtools** to check alignment of raw Illumina reads to the assembled genome. If the assembly is correct and no elements are not being assembled, you would expect a mapping rate of **99%**.


### **4) Inferring breakpoint for incorrectly assembled reads**

Inferring breakpoints for incorrectly assembled reads with ALLMAPS is not very accurate when using only a few thousand SNPS, therefore one may use [**PolarSTAR**](https://github.com/phasegenomics/polar_star) to infer breakpoints based on support (or lack of support) from long-read data.


### **5) Genome scaffolding with genetic linkage maps**

This step should be performed before repeatmasking if planning on using the genome for annotation.

Genome scaffolding was performed using **ALLMAPS** using the following steps:

1) SNP probes were blasted aganst the assembled genome using **BLAST** to find the position of the SNPs used to construct high-density genetic linkage maps.

2) SNP probe positions were extracted and used in a **.csv** file as specified on the [**ALLMAPS**](https://github.com/tanghaibao/jcvi/wiki/ALLMAPS) webpage.

3) Run **ALLMAPS** script found in **Hybrid_assembly_Canu_binned/ALLMAPS** directory.


## **8) Structural Variant (SV) identification and analysis**

This is described under the Final Thesis Methods folder. Final process of RE masking is also found here.
