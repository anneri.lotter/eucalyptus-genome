#!/bin/bash
#SBATCH --job-name=quast_long
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/%x_%A.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/%x_%A.err

hostname
module load quast/5.0.2

# flye statistics
#quast.py /labs/Wegrzyn/Moss/Physcomitrellopsis_africana/Physcomitrellopsis_africana_Genome/RawData_Nanopore_5074/5074_test_LSK109_30JAN19/flye_assembly/flye_assembly_initial/assembly.fasta -o Flye

# shasta statistics
#quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Shasta2/Assembly.fasta -o Shasta2_quast

#masurca stats
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -o Masurca1_quast
