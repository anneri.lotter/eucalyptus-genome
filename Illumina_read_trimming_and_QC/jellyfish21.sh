#!/bin/bash
#SBATCH --job-name=jf21
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 28
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o jf21_%j.out
#SBATCH -e jf21_%j.err

module load jellyfish/2.2.6
jellyfish count -t 28 -C -m 21 -s 100G -o 21mer_out --min-qual-char=? /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_unclassified_1.fastq /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_unclassified_2.fastq
jellyfish histo -o 21mer_out.histo 21mer_out

cp -v 21mer_out.histo ~/
