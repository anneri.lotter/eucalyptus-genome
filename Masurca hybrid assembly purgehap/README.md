# Removal of redundant reads

To remove redundant reads (in this case the alternative haplotype) from the Masurca hybrid assembly, **Purge Haplotigs** was used. This step was also performed for the FALCON based genome assembly (found in the **Long-read genome assembly/Falcon Genome assembly** directory.

**Note:** Only one line is run at a time, after which the script must be adapted from the histogram results

**Input:**

*	**purgehap.sh** script to run Purge Haplotigs
*	A bam file containing raw reads aligned to the genome you want to purge

**Output:**

*	Histogram png picture. Refer to the [PurgeHap web page](https://bitbucket.org/mroachawri/purge_haplotigs/src/master/) to find guidelines for choosing the low (**-l**), medium (**-m**) and high (**-h**) coverage cutoffs
*	A draft purged assembly

The resulting purged assembly can be assessed with QUAST and BUSCO. The assembly should have a lower Duplicated BUSCO score as well as a reduced total genome size.

A link to the resulting histogram picture is provided on the home page.
