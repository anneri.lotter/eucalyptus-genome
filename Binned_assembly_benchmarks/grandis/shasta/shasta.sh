#!/bin/bash
#SBATCH --job-name=shasta_gra
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=450G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load shasta/0.7.0
shasta --input /core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/haplotype-GRA_rmv_contam_reads.fasta --assemblyDirectory ./shasta_gra --Reads.minReadLength 500 --memoryMode anonymous --memoryBacking 4K --threads 16

