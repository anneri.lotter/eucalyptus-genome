#!/bin/bash
#SBATCH --job-name=kraken_raw_illumina_reads
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=200G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err
​
module load kraken/2.0.8-beta
module load jellyfish/2.2.6
​
kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /projects/EBP/CBC/eucalyptus/rawReads/Illumina/F1_FK118_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/F1_FK118_2.fastq.gz \
        --use-names \
        --threads 8 \
        --output FK118_kraken_general.out \
        --unclassified-out FK118_unclassified#.fastq \
        --classified-out FK118_classified#.fastq      \
        --report FK118_report_Kraken_report.txt \
        --use-mpa-style
​
kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_2.fastq.gz \
        --use-names \
        --threads 8 \
        --output GRA_FK1758.out \
        --unclassified-out GRA_FK1758_unclassified#.fastq \
        --classified-out GRA_FK1758_classified#.fastq      \
        --report GRA_FK1758_Kraken_report.txt \
        --use-mpa-style
​
kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_2.fastq.gz \
        --use-names \
        --threads 8 \
        --output URO_FK1556_general.out \
        --unclassified-out URO_FK1556_unclassified#.fastq \
        --classified-out URO_FK1556_classified#.fastq      \
        --report URO_FK1556_Kraken_report.txt \
        --use-mpa-style
​
