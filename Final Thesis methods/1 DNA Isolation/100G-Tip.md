### 100 G/Tip Qiagen

1. Start with 1 grams of ground samples (liquid nitrogen frozen samples)
2. Add 8 ml of Guanidine extraction buffer (recipe below)
3. Add 5 mg of Cellulase, 10 mg of Lysing enzyme
4. Incubate for 2 hours at 42°C
5. Supplement with DNase-free RNase A (20 μg/ml) and incubate for 30 min at 37°C.
6. Centrifuge for 20 min at 12–15,000 x g to pellet insoluble debris.
7. Transfer clarified lysate to the appropriate buffer QBT-equilibrated QIAGEN Genomictip (Genomic-tip 20/G: 1 ml; 100/G: 3 ml; 500/G 10 ml).
8. Wash with Buffer QC (Genomic-tip 20/G: 4 x 1 ml; Genomic-tip 100/G: 2 x 10 ml;Genomic-tip 500/G: 2 x 30 ml). 
9. Elute with Buffer QF (Genomic-tip 20/G: 0.8 ml; Genomic-tip 100/G: 5 ml; Genomic-tip 500/G: 15 ml). 
10. Add Proteinase K to 0.8 mg/ml and incubate for 2 h at 50°C with gentle agitation.

|Guanidine extraction buffer: | |
| ------ | ------ |
| 20 mM  | EDTA |
| 100 mM  | NaCl |
| 1 % | Triton® X-100 |
| 500 mM | Guanidine-HCl |
| 10 mM | Tris, pH 7.9 |
