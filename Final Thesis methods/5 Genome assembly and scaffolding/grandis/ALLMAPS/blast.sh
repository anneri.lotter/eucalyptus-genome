#!/bin/bash
#SBATCH --job-name=blast_gra
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load blast/2.7.1
makeblastdb -in gra_PS_splitted.fa -dbtype nucl -out gra_db
blastn -num_threads 16 -query ../matched_gra1.fasta -db gra_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out Gra_SNP_Genome.blastp
blastn -num_threads 16 -query ../matched_uro1.fasta -db gra_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out Uro_SNP_genome.blastp

awk '{print $1"\t"$2"\t"$9"\t"$10 }' Gra_SNP_Genome.blastp > Gra_SNP_Probes_to_Assembly.txt
awk '{print $1"\t"$2"\t"$9"\t"$10 }' Uro_SNP_genome.blastp > Uro_SNP_Probes_to_Assembly.txt

awk 'BEGIN { FS = OFS = "\t" } NR == FNR {fn[$1] = $2; next} {print $2, $3, ($1 in fn ? fn[$1] : "FALSE")}' grandis_map_SNPs_map.txt Gra_SNP_Probes_to_Assembly.txt | sed 's/%/\t/' > gra_allmap.file
awk 'BEGIN { FS = OFS = "\t" } NR == FNR {fn[$1] = $2; next} {print $2, $3, ($1 in fn ? fn[$1] : "FALSE")}' urophylla_map_SNPS_map.txt Uro_SNP_Probes_to_Assembly.txt | sed 's/%/\t/' > uro_allmap.file

tr '\t' ',' < gra_allmap.file > gra_allmap.csv
tr '\t' ',' < uro_allmap.file > uro_allmap.csv
