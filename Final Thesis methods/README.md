# Haplotype-resolved genome assembly of an F1 hybrid of *Eucalyptus urophylla* x *E. grandis*

Repository for methods and scripts related to the *Eucalyptus* genome assembly and SV analysis. Scripts mentioned below each step can be found in the named folder. 

Make note of the bash script headers as some jobs were run on different cluster systems.

Results from scripts can be found in this [Spreadsheet](https://docs.google.com/spreadsheets/d/1iKUW9isPEPHXHKSJ6FPSzR1E_ramQQEuj41ncGL07w0/edit?usp=sharing)


## **Project background**

Current breeding strategies for eucalypt tree improvement are less accurate than haplotype-based molecular breeding strategies being deployed in other crop species. However, to use haplotype-based molecular breeding strategies for tree improvement, a high-quality reference genome is required for the species. The current *E. grandis* genome has been assembled using short-read technologies, which pose a variety of challenges and result in fragmented and incomplete reference genomes. Towards this, long-read sequencing technologies offer a solution to many of these challenges, and can be used to improve / assemble high quality reference genomes. In addition, long-read sequencing has been used in combination with short-read sequencing, to obtain high quality reference genomes for two parental species by sequencing an interspecific hybrid. This reduces the cost of generating such assemblies, as fewer sequence information is required than when these genomes are assembled independently.

As such, this project aims to assemble the genome of an F1 *E. urophylla* x *E. grandis* hybrid, to assemble high-quality reference genomes for both *E. urophylla* and *E. grandis*.

In order to obtain the best possible genome, multiple genome assembly programs will be tested as well as a trio-binning approach for haplotype assembly of *E. grandis* and *E. urophylla* as discussed in a paper by Koren *et al* 2018.

## **1) DNA Isolation**

#### **Illumina sequencing**

DNA for short-read sequencing was extracted using a modified Machery-Nagel NucleoSpin® Plant II protocol. 

#### **Nanopore sequencing**

DNA for Nanopore sequencing was extracted using two methods (SDS-based and Qiagen 100G-Tip) for MinION sequencing after which DNA extracted with the 100G-Tip was sent for PromethION sequencing.


## **2) DNA Sequencing**

DNA was extracted from leaf tissue of and F1 *E. urophylla* x *E. grandis* hybrid. Sequencing of the hybrid
was performed using the ONT MinION and PromethION v9.4 platform.

Raw long-read sequencing data located at:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/PromethION

Additionally, to use the trio-binning approach proposed by Koren *et al*, 151 bp PE Illumina sequencing data was generated for the F1 hybrid, as well as the *E. urophylla* and *E. grandis* parents.

Raw short-read sequencing data can be found at:

*       /projects/EBP/CBC/eucalyptus/rawReads/Illumina

The data generated for both platforms are displayed below, along with the theoretical estimated genome coverage:

**Illumina sequencing data:**

*	*E. urophylla* parent: 127.5 Gb, 196.2x
*	*E. grandis* parent: 141.6 Gb, 217.8x
*	F1 Hybrid: 116.1 Gb, 178.61x

**Nanopore sequencing data:**

*	MinION Run1: 11.18 Gb called, 9.5 Gb passed
*	MinION Run2: 2.55 Gb called, 2.32 Gb passed
*	PromethION: 61.59 Gb called, 56.69 Gb passed
*	Total: 75.32 Gb called, 68.51 Gb passed

Total estimated ONT coverage: 105.4x


## **3) Read basecalling and QC**
 
#### **Illumina sequencing data**

Basic Illumina read QC was performed with **FastQC**, note that raw data was used as input for read binning with **Canu**.

Genome size was estimated for the F1 hybrid and both parents based on short-read K-mer analysis using **Jellyfish v2.0** and visualizing with **GenomeScopev2.0**.

**Input (FastQC):**

* Raw **fastq** files
* **FASTQC.sh** script to run FASTQC

**Output:**

* FASTQC report file for each read set

**Input (Jellyfish):**

* Raw **fastq** files
* **jellyfish.sh** script to run jellyfish

**Output:**

* **.histo** file containing histogram input for GenomeScope

GenomeScope results can be viewed here for the following individuals:

*	[F1 hybrid](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=OD5jAu9BCy7MoIFHmkOO)
*	[*E. urophylla* parent](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=mXVEIQxHMIpGFqh7gzk5)
*	[*E. grandis* parent](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=cb6smCH0wSyDxu7dpSGu)

Using the .histo file as input for [genomescope2](http://qb.cshl.edu/genomescope/genomescope2.0/), use the default parameters and only input the kmer length and ploidy (n = 2 for *Eucalyptus*).

#### **ONT sequencing data**

Basecalling was performed using the **Guppy basecaller** (GPU version). This was followed with removal of sequencing adapters was performed using **PoreChop**. 

**Input (Guppy):**

* **FAST5** read files from ONT sequencing
* **guppy.sh** script to run Guppy 

**Output:**

* **fastq** read files sorted according to passed and failed read QC value (greater than 7)

**Input (PoreChop):**

* **fastq** read files that passed QC (use cat to put all fastq files from guppy into a single fastq file for further use)
* **porechop.sh** scropt to run porechop

**Output:**

* **fastq** read files with removed adapters


## **4) Read binning contaminant removal**

#### **Trio-binning of F1 ONT reads**

Binning of ONT reads of the F1 hybrid into *E. urophylla* and *E. grandis* bins was performed using [**Canu**]( https://canu.readthedocs.io/en/latest/quick-start.html)

**Input:**

* Raw **fastq** Illumina short read files of parents
* Raw **fastq** ONT file of F1 hybrid
* **canu.sh** script to bin reads

**Output:**

* Binned **fastq** long-read files

Removal of contaminant reads (reads that are from organisms other than the species being sequenced) was performed using **Kraken2** (Illumina) and **Centrifuge** (ONT).

**Input (Kraken2):**

*	PE Illumina reads short reads in **fastq** format
*	Kraken classification database 
*	**kraken.sh** script for generating unclassified and classified read sets

**Output:**

*	Classified and unclassified short read data for each read set (we will want to use the unclassified set going forward)

**Input (Centrifuge):**

*	Raw long reads in **fastq** format
*	Sequence index database of possible contaminants (human, archaea, bacterial and fungal)
*	**centrifuge.sh** script to generate a classification report

**Output:**

*	Centrifuge classification report

**Note:** Additional python scripts must be used to remove contaminated reads identified in the report. 

Resulting reports can be found on the spreadsheet.


## **5) Genome assembly and scaffolding**

#### Masurca assembly of Canu binned reads

Binned long-reads from Canu were assembled independantly using the Masurca hybrid genome assembler (using uncontaminated parental short reads and binned long-reads of the F1 hybrid). Two separate assemblies were run from this point on, one per bin, thusthe scripts are split into the relevant species folder.

**Input:**

*	Uncontaminated and binned ONT long reads in **fasta** format
*	Unclassified PE Illumina parental short reads in **fastq** format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh file to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory


#### Checking assembly size

To check if the assembled genome size is correct (and that the reduced assembly size compared to the estimated size is not due to reads not being assembled), **bwa mem** can be used along with the **flagstat** function from **samtools** to check alignment of raw Illumina reads to the assembled genome. If the assembly is correct and no elements are not being assembled, you would expect a mapping rate of **99%**.

**Input:**
*	**bwa_align.sh** script to run bwa mem and calculate alignment statistics

**Output:**
*	**.sam** and **.bam** alignments files containg raw Illumina reads aligned to the assembled genome
*	**stats.out** file containing the alignment statistics. If all the reads were used a 99% alignment score is expected.


#### **Inferring breakpoints for incorrectly assembled reads**

Breakpoints were inferred for incorrectly assembled contigs with [PolarSTAR](https://github.com/phasegenomics/polar_star) based on support (or lack of support) from long-read data.

**Input:**
*   **config.json** file specifying the location of the binned reads and the genome to be validated
*   **polarstar.sh** script to run polarstar
*   Need to set up a conda evironment to for polarstar to run in as specified on their [website](https://github.com/phasegenomics/polar_star/blob/master/README.md)
*   Raw uncontaminated long-reads in **fasta** format
*   Assembly in **fasta** format

**Output:**
*   **new_fasta.fa** containing split contigs

Note: All reads of 3 Kb were removed before genome scaffolding. One can do this either way described below with seqtk or the seqkit tool.
   
    seqkit seq -m 3000 your_fasta.fa > 3Kb.fasta
    seqtk seq -L 3000 contigs.fasta > 3Kb.fasta               

To check if scaffolds were assembled correctly, one would align raw reads to the scaffolded assembly and check for errors via IGV and erraneous contigs can be broken manually (the breakpoint can be discovered via alignment of raw reads to the erraneous assembled area with MUMmer). This was done for _E. grandis_ after scaffolding revealed an error on chromosome 6.

#### **Genome scaffolding**

After genome assembly with Masurca and inference of breakpoint, the genome can be scaffolded using high-density genetic linkage maps using the **ALLMAPS** program. 

To do so the following is required to generate input files for ALLMAPS:

1) Create a file containing SNP names and probes with awk

*    `awk '{print $1"\t"$2}' SNP Probes.txt > snp_probe_name.txt`

2) Convert probe files to .fasta files for use in BLAST

*    `awk '{ printf ">%s\n%s\n",$1,$2 }' matched_uro1.txt > matched_uro1.fasta`
*    `awk '{ printf ">%s\n%s\n",$1,$2 }' matched_gra1.txt > matched_gra1.fasta`

3) **BLAST** the SNP probe sequences against the assembled genome to find their genetic position.

**Input:**

*	Draft genome assembly in **fasta** format
*	**blast.sh** script (in relevant directory)

**Output:**

*	Database to blast against, consisting of your assembled genome
*	**.blastp** file containing the following info/headers: qseqid (query seq), sseqid (ref genome), pident (% identity match), length (of alignment), mismatch (number mismatches), gapopen (number gap openings), qstart (query alignment start), qend (query alignment end), sstart (ref alignment start), send (ref alignment end), evalue, bitscore


4) Generate a .csv file containing the scaffold ID, scaffold position (this will correspond to the SNP position on the scaffold), the LG (linkage group to which the scaffold belongs, from the genetic linkage map) and the position (physical position as in the gentic map).

*    `awk '{print $1"\t"$2"\t"$9"\t"$10 }' SNP_Genome.blastp > UroSNP_Probes_to_Assembly.txt`
*    `awk '{print $3"\t"$1":"$4}' urophylla_map_SNPS.txt > urophylla_map_SNPS_map.txt`
*    `awk 'BEGIN { FS = OFS = "\t" } NR == FNR {fn[$1] = $2; next} {print $2, $3, ($1 in fn ? fn[$1] : "FALSE")}' urophylla_map_SNPS_map.txt UroSNP_Probes_to_Assembly.txt | sed 's/%/\t/' > AllMap1.file`
*    `tr '\t' ',' < AllMap1.file > UroAllMap1.final.csv`

Note that I used the SNP probe start position and not the start to end position.

6) Using a text editor, insert the headers as specified on the ALLMAPS github page.

In this case we specify Scaffold ID,scaffold position,LG,genetic position.

7) Run ALLMAPS by running the **run.sh** script

**Input:**

*	**.csv** file of genetic linkage maps (preferably both parents as this increase scaffolding rate of contigs)
*	**run.sh** script to run scaffolding
*	Draft genome assembly file in **fasta** format

**Output:**

*	**.bed** file containing the SNP positions for both genetic linkage maps
*	**weight.txt** file containing the weight each genetic map will be given (in this case this will be adjusted depending on the haplotype being assembled so the parental haplotype carries greater weight i.e. pivot map)
*	**fasta** file woth reconstructed chromosome sequences
*	PDF files with alignment visualisations corresponding to reconstructed chromosomes
*	**summary.txt** file containing information regarding number of scaffold anchored, N50 etc.

NOTE: You can also estimate gap lengths and redo the assembly.


#### **Genome assembly quality**

Assembled genomes were evaluated for quality after each step using **BUSCO** to identify the number of core genes present in the assembled genome, and **QUAST** to check genome assembly statistics such as N50, # contigs etc.

All follow this layout:

*	BUSCO: run_BUSCO.py -i /path/to/assembly.fasta -l /path/to/embryophyte/data -o /name_of_output_folder -m geno 

*	QUAST: quast.py /path/to/assembly.fasta -o Name_of_output_folder

Results are provided on the spreadsheet.


## **6) Synteny and Structural rearrangements (SyRI)**

Masking of repeat elements is recommended before SV identification in large genomes like wheat to reduce runtimes, however I did not use a masked genome as input for this study. ONLY THE CHROMOSOMES ARE USED AS INPUT.

**Input:**
*   **fasta** files of chromosomes of the genomes to be compared 
*   **run.sh** script to run [SyRI](https://github.com/schneebergerlab/syri)

**Output:**
*   Many **txt** files with different SV and local variant calls
*   An **out** file used for plotting results - this file can be used for visualization with Circos
*   A **pdf** file containing a linear alignment of syntenic and rearranged regions (svg can also be the selected output)
*   A **summary** file that summarizes the total number of variants and their total size
*   **.delta** file - this can be filtered and used for other SV callers such as Assemblytics


## **7) Masking of repetitive elements**

This step is recommended before genome annotation. After repetitive elements have been masked one can obtain an estimate of the genome repetitiveness, as well as the type of elements involved in repetitiveness.

The first step in annotation is to softmask the genome. This finds repeated elements in the genome and changes those nucleotides to lowercase letters (i.e. ATCG to atcg). Some of the analysis tools down the line consider this information. 

#### **1) RepeatModeler**

To begin the masking process, we must first use **RepeatModeler** on the filtered data.

**RepeatModeler** will output a set of **consensi** files, which are used in **RepeatMasker**.

**Input:**

*	**repeatmodeler_*.sh** script to run repeatmodeler
*	Assembled genome file in **fasta** format

**Output:**

*	A **consensi.fa** file containing repeat element database for masking


#### **2) RepeatMasker**

To mask repeat elements based on the repeat library, we use **RepeatMasker**. In this study, we combined the libraries that were constructed with RepeatModeler using cat. We used a combined library as using only the library constructed for the species resulted in ommision of some elements.

**Input:**

*	Assembled genome in **fasta** format
*	**repeatmasker_*.sh** script to mask the assembled genome
*   **consensi.fa** repeat library (the combined library)

**Output:**

*	Masked genome in **fasta** format
*	**.tbl** file which contains a summary of masking percentage and percentage of element classes for the genome 
*   **.out** file with alignment information

#### **3) LTR_Retriever**

To identify LTR elements and classes, LTR_retriever can be used as it is specifically set up for plant LTR detection which may have been missed or classified as unknown by RepeatModeler.

**Input:**

*   Assembled genome in **fasta** format
*   **ltr_*.sh** script to run the various programs for LTR_Retriever
-   LTR_Retriever needs candidates identified with LTR_Finder and LTRHarvest which is used as an input

**Output:**
*   Multiple **scn** and othe intermediate files that are input files for LTR_Retriever
*   **.tbl** file that contains the total percentage of LTR elements found (similar to that generated by RepeatMasker)
*   **.LTR.distribution.txt** file with the distribution of LTR elements in bins of all scaffolds 
-   This can be used to visualize distribution with Circos or R


## **8) Circos**

[**Circos**](http://circos.ca/) was used to visualize the distribution of different LTR elements identified with LTR_Retriever.
This was also used to visualize synteny and rearrangements (greater than 10 Kb) that were identified with SyRI.

For LTRs I split the three classes for mapping using the awk to print the relevant columns from the **.LTR.distribution.txt** file.

Using the same command I also prepared the synteny and rearrangements input files.


**Input:**
*   **txt** file with the information to map with the following format: 1) for LTRs chromosome, start, end, value and 2) for synteny and SV chromosomeA, startA, startB, chromosomeB, startB, endB
    NOTE that for the chromosome column, to map the two genomes, the chromosome name must differ (I used Chr and EUChr for *E. grandis* and *E. urophylla*). This can be accomplished using the sed command.
*   **combined.conf** script specifying what to plot
*   **labels_*.txt** file
*   **karyotype_*.txt** file

**Output:**
*   **png** and **svg** pictures

For SV, I filtered thet input files for SV > 10 Kb using awk. The SV.conf file was used (note each SV was mapped separately).

## **9) Supplementary notes**
All methods related to supplementary notes 1 to 3 are in this directory and are explained in the README file in this directory.







