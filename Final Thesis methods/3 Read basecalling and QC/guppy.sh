#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=24:00:00
#PBS -q gpu
#PBS -o /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/gpu_stdout.log
#PBS -e /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/gpu_stderr.log
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za

# Guppy Run1
guppy_basecaller --flowcell FLO-MIN106 --kit SQK-LSK109 \
                 --input_path /nlustre/users/anneri/EucGUF1_MinIONruns/EucGUF1_MinION_100Gtip_22Aug2019/GUF1/20190820_0950_MN28560_FAK49733_0cfae16a/fast5 \
                 --save_path /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/ \
                 --num_callers 28 -x "cuda:0" --cpu_threads_per_caller 1 \
                 --min_qscore 7.0 --qscore_filtering 8.0 --records_per_fastq 0 \
                 --enable_trimming yes --trim_threshold 2.5 --trim_min_events 3 \
                 --recursive

# Guppy F1GU_Run2
guppy_basecaller --flowcell FLO-MIN106 --kit SQK-LSK109 \
                 --input_path /nlustre/users/anneri/EucGUF1_MinIONruns/EucGUF1_MinION_SDSbased_24Aug2019/Run_2/20190822_1438_MN28560_FAK49611_53b4dc4a/fast5 \
                 --save_path /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/MinION_Run02_GPU/ \
                 --num_callers 28 -x "cuda:0" --cpu_threads_per_caller 1 \
                 --min_qscore 7.0 --qscore_filtering 8.0 --records_per_fastq 0 \
                 --enable_trimming yes --trim_threshold 2.5 --trim_min_events 3 \
                 --recursive

