#!/bin/bash
#SBATCH --job-name=rpm_ltr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general 
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o repeatmasker-%j.output 
#SBATCH -e repeatmasker-%j.error 

# Run the program                 
echo gra_PS.fasta

#module load RepeatMasker/4.0.6 
#module load perl/5.24.0

#module load RepeatMasker/4.1.1
module load RepeatMasker/4.0.9-p2
#export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5
#export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

reference='gra_PS.fasta'
lib='uro_all_lib1.fa' #created by RepeatModeler (consensi.fa.classified) and LTR_retriever (*.LTRlib.fa)
outputDir="02_repeatMasker_rpm2/"
genomeSize=650M
#use 4 per one threads, if set it to 10, it will use 40
threads=10

divPath=$outputDir/$GENOME_NAME.repeats.divsum

if [ ! -d $outputDir ]; then
    mkdir $outputDir
fi

rm $outputDir/*

ln -s $reference $outputDir

echo 'begin to run repeatMasker'

RepeatMasker gra_PS.fasta -dir 02_repeatMasker_rpm2/ -lib ./RM_60772.MonOct120722412020/uro_all_lib1.fa $outputDir/$(basename $reference) -pa 4 -gff -a -noisy -nolow -xsmall 1>RepeatMaskerrpm2.log 2>RepeatMaskerrpm2.err


# The pipe "|" here creates an output text file that can be checked for errors
#RepeatMasker gra_PS.fasta -dir RM_60772.MonOct120722412020/repeatmasker_out_clas -lib RM_60772.MonOct120722412020/consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall| tee gra_PS.txt

#RepeatMasker gra_PS_split_AM.modified.fasta -dir /gra_ltr/RM_ltr -lib /gra_ltr/gra_PS_split_AM.modified.fasta.mod.LTRlib.fa -pa 4 -gff -a -noisy -low -xsmall| tee gra_PS_ltr.txt
/isg/shared/apps/RepeatMasker/4.0.9-p2/util/calcDivergenceFromAlign.pl -s $divPath $outputDir/$(basename $reference).align

/isg/shared/apps/RepeatMasker/4.0.9-p2/util/createRepeatLandscape.pl -div $divPath -g $genomeSize > $outputDir/$GENOME_NAME.repeats.html

