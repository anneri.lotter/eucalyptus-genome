#!/bin/bash
#PBS -l nodes=2:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/anneri/repeatmasking/result_uro/
#PBS -e /nlustre/users/anneri/repeatmasking/result_uro/
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za

module load ltr_finder-1.0.6

cd /nlustre/users/anneri/repeatmasking/result_uro/

perl -nle 's/scf718000000//g; print $_' uro.PS_AM_unmasked.fasta > uro.PS_AM_unmasked.modified.fasta

#ltr_finder
ltr_finder -D 15000 -d 1000 -L 7000 -l 100 -p 20 -C -M 0.9 uro.PS_AM_unmasked.modified.fasta > uro.PS_AM_unmasked.finder.scn

#ltr_finder -D 15000 -d 1000 -L 7000 -l 100 -p 20 -C -M 0.9 gra_PS_split_AM.fasta -w 1 2 > LTRfinder.out

module load genometools-1.5.9
#ltrharvest
gt suffixerator -db uro.PS_AM_unmasked.modified.fasta -indexname uro.PS_AM_unmasked.modified.fasta -tis -suf -lcp -des -ssp -sds -dna

gt ltrharvest -index uro.PS_AM_unmasked.modified.fasta -similar 90 -vic 10 -seed 20 -seqids yes -minlenltr 100 -maxlenltr 7000 -mintsd 4 -maxtsd 6 -motif TGCA -motifmis 1 > uro.PS_AM_unmasked.harvest.scn

gt ltrharvest -index uro.PS_AM_unmasked.modified.fasta -similar 90 -vic 10 -seed 20 -seqids yes -minlenltr 100 -maxlenltr 7000 -mintsd 4 -maxtsd 6 > uro.PS_AM_unmasked.harvest.nonTGCA.scn


module load ltr_retriever
module load maker-2.31.10
module load cdhit
module load hmmer-3.1b2

#run LTR_retriever to get the LAI score  
LTR_retriever -genome uro.PS_AM_unmasked.modified.fasta -inharvest uro.PS_AM_unmasked.harvest.scn -infinder uro.PS_AM_unmasked.finder.scn -nonTGCA uro.PS_AM_unmasked.harvest.nonTGCA.scn -threads 20

