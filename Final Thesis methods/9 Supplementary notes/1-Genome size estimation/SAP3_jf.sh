#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o /nlustre/users/anneri/genome_size/WGS_data/SAP3/jellyfish.o
#PBS -e /nlustre/users/anneri/genome_size/WGS_data/SAP3/jellyfish.e
#PBS -k oe
#PBS -m ae
#PBS -M anneri.lotter@fabi.up.ac.za


module load jellyfish-2.3.0

cd /nlustre/users/anneri/genome_size/WGS_data/SAP3/
jellyfish count -t 28 -C -m 21 -s 100G -o GUF1_A0380_21mer_out --min-qual-char=? A0380_1.fq A0380_2.fq
jellyfish histo -o GUF1_A0380_21mer_out.histo GUF1_A0380_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o GRA_H1701_21mer_out --min-qual-char=? H1701_1.fq H1701_2.fq
jellyfish histo -o GRA_H1701_21mer_out.histo GRA_H1701_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o GRAbc_P1381_21mer_out --min-qual-char=? P1381_1.fq P1381_2.fq
jellyfish histo -o GRAbc_P1381_21mer_out.histo GRAbc_P1381_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o URObc_M1459_21mer_out --min-qual-char=? M1459_1.fq M1459_2.fq
jellyfish histo -o URObc_M1459_21mer_out.histo URObc_M1459_21mer_out
