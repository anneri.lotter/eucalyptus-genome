#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o //nlustre/users/anneri/HiSeq/jellyfish.o
#PBS -e /nlustre/users/anneri/HiSeq/jellyfish.e
#PBS -k oe
#PBS -m ae
#PBS -M anneri.lotter@fabi.up.ac.za


module load jellyfish-2.3.0

cd /nlustre/users/anneri/HiSeq
for f in *.gz; do gunzip -dv "$f"; done

jellyfish count -t 28 -C -m 21 -s 100G -o FK1755_21mer_out --min-qual-char=? URO_FK1755_1.fastq URO_FK1755_2.fastq
jellyfish histo -o FK1755_21mer_out.histo FK1755_21mer_out


module load seqtk
seqtk sample -s100 F1_FK118_1.fastq 74943555 > F118_sub_1.fastq
seqtk sample -s100 F1_FK118_2.fastq 74943555 > F118_sub_2.fastq


jellyfish count -t 28 -C -m 21 -s 100G -o F118_sub_21mer_out --min-qual-char=? F118_sub_1.fastq F118_sub_2.fastq
jellyfish histo -o F118_sub_21mer_out.histo F118_sub_21mer_out

seqtk sample -s100 GRA_FK1758_1.fastq 74943555 > GRA_FK1758_sub_1.fastq
seqtk sample -s100 GRA_FK1758_2.fastq 74943555 > GRA_FK1758_sub_2.fastq


jellyfish count -t 28 -C -m 21 -s 100G -o GRA_FK1758_sub_1_21mer_out --min-qual-char=? GRA_FK1758_sub_1.fastq GRA_FK1758_sub_2.fastq
jellyfish histo -o GRA_FK1758_sub_1_21mer_out.histo GRA_FK1758_sub_1_21mer_out

seqtk sample -s100 URO_FK1556_1.fastq 74943555 > URO_FK1556_sub_1.fastq
seqtk sample -s100 URO_FK1556_2.fastq 74943555 > URO_FK1556_sub_2.fastq

jellyfish count -t 28 -C -m 21 -s 100G -o URO_FK1556_sub_21mer_out --min-qual-char=? URO_FK1556_sub_1.fastq URO_FK1556_sub_2.fastq
jellyfish histo -o URO_FK1556_sub_21mer_out.histo URO_FK1556_sub_21mer_out

seqtk sample -s100 URO_FK1755_1.fastq 74943555 > URO_FK1755_sub_1.fastq
seqtk sample -s100 URO_FK1755_2.fastq 74943555 > URO_FK1755_sub_2.fastq

jellyfish count -t 28 -C -m 21 -s 100G -o URO_FK1755_sub_21mer_out --min-qual-char=? URO_FK1755_sub_1.fastq URO_FK1755_sub_2.fastq
jellyfish histo -o URO_FK1755_sub_21mer_out.histo URO_FK1755_sub_21mer_out

seqtk sample -s100 55871_1.fastq 74943555 > 55871_sub_1.fastq
seqtk sample -s100 55871_2.fastq 74943555 > 55871_sub_2.fastq


jellyfish count -t 28 -C -m 21 -s 100G -o 55871_sub_21mer_out --min-qual-char=? 55871_sub_1.fastq 55871_sub_2.fastq
jellyfish histo -o 55871_sub_21mer_out.histo 55871_sub_21mer_out

seqtk sample -s100 55901_1.fastq 74943555 > 55901_sub_1.fastq
seqtk sample -s100 55901_2.fastq 74943555 > 55901_sub_2.fastq


jellyfish count -t 28 -C -m 21 -s 100G -o 55901_sub_1_21mer_out --min-qual-char=? 55901_sub_1.fastq 55901_sub_2.fastq
jellyfish histo -o 55901_sub_1_21mer_out.histo 55901_sub_1_21mer_out

seqtk sample -s100 NN0784_1.fastq 74943555 > NN0784_sub_1.fastq
seqtk sample -s100 NN0784_2.fastq 74943555 > NN0784_sub_2.fastq

jellyfish count -t 28 -C -m 21 -s 100G -o NN0784_sub_21mer_out --min-qual-char=? NN0784_sub_1.fastq NN0784_sub_2.fastq
jellyfish histo -o NN0784_sub_21mer_out.histo NN0784_sub_21mer_out

seqtk sample -s100 NN2868_1.fastq 74943555 > NN2868_sub_1.fastq
seqtk sample -s100 NN2868_2.fastq 74943555 > NN2868_sub_2.fastq

jellyfish count -t 28 -C -m 21 -s 100G -o NN2868_sub_21mer_out --min-qual-char=? NN2868_sub_1.fastq NN2868_sub_2.fastq
jellyfish histo -o NN2868_sub_21mer_out.histo NN2868_sub_21mer_out
