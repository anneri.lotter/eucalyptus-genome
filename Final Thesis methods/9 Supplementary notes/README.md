## **1) Supplementary Note 2.1: Variation in genome size based on k-mer analyses in _E. grandis_, _E. urophylla_ and _E. dunnii_**

To further investigate whether the estimated genome size based on k-mer analysis was within the size range for _E. grandis_, _E. urophylla_ and _E. grandis_ x _E. urophylla_ (GU) F1 hybrids, we performed k-mer based genome size estimation using Jellyfish v2.3.0 for 21-mers and visualized with GenomeScope1.0 for a number of Illumina whole genome sequencing datasets produced previously in our laboratory for individuals of key eucalypt tree species, both from unimproved and improved material (unpublished results). We compared estimates for 25 E_. grandis_, 19 _E. dunnii_, three _E. urophylla_, four GU F1 hybrid and one F1 GU x _E. urophylla_ (GU x U) backcross individual. Most of these samples’ sequencing data consisted of 100 bp (PE100) Illumina sequencing reads and had only 19 – 40x genome coverage, whereas those from this study and other individuals within this NAM population had a read length of 150 bp (PE150) and at least 124x coverage. We used version 1.0 of GenomeScope due to low genome coverage and the ability to adjust for read length.

**Input (Jellyfish):**

* Raw **fastq** files
* All scripts in the 1-Genome size estimation directory to run jellyfish for the samples

**Output:**

* **.histo** file containing histogram input for GenomeScope

For samples with >100X genome coverage, we also performed the same ana;ysis on a subset of 25G of the total sequencing data using **seqtk**:

**Input: **

* Raw **fastq** files
* The **jellyfish.sh** script to subset paired reads and run jellyfish on the subset reads

**Output: **

* *.histo** file containing histogram input for GenomeScope

Using the .histo file as input for [genomescope](http://qb.cshl.edu/genomescope), use the default parameters changing the read length and max k-mer coverage of 1,000 and 10,000 as a separate run. A summary of the run results can be viewed [here](https://docs.google.com/spreadsheets/d/1iKUW9isPEPHXHKSJ6FPSzR1E_ramQQEuj41ncGL07w0/edit?usp=sharing).

The histo files for all runs can ba found [here](https://drive.google.com/drive/folders/1E2ViDIjsT5CFzBgQ94PFNwBHDC8Od4rB?usp=sharing) for full datasets and [here](https://drive.google.com/drive/folders/1CldYggqkHDEt9Xh5-cJ4WGB2eYTp63eG?usp=sharing) for the subset data.


## **2) Supplementary Note 2.2: Hap-mer based phasing completeness assessment**

To evaluate the size of phased block and phase correctness of our haplo_genome assemblies, we used [Merqury v1.0](https://github.com/marbl/merqury). Merqury basically check whether the haplotype specific k-mers found in the short-read dataset before binning corresponds to those found in the assembly, and whether the correct parental k-mers are present in the assembly.

**Input: **

* Subset of the meryl database created by **Canu** (the parent specific mers used for haplotype binning, please see [merqury website](https://github.com/marbl/merqury/wiki/1.-Prepare-meryl-dbs) for more details)
* **merqury.sh** script to create child meryl database and run merqury

**Output: **

* A bunch of files are generated, but we are interested in:
    *   ***.hapmers.count** file used to generate blob plots and the blob plots
    *   ***.100_20000.phased_block.sizes** and ***.10_20000.phased_block.sizes** used to generate phase block stats at 100 and 10 (strict) switch errors per 20 kb block
    *   ***.contig/scaffold/phased_block.sizes** to check phase block sizes compared to contig/scaffold sizes

## **3) Supplementary Note 2.3:  Read and assembly alignment and validation of high peak content**

First we aligned short-read and long-read binned sequencing data to the _E. grandis_ v2.0 reference genome with **minimap**. We then convert the resulting SAM file to BAM with **SAMtools** and get coverage in 100 kb bins with **deeptools**. The resulting bed files were visualised in IGV browser and high coverage bins identified visually. 


Within the high coverage bins we looked for the boundaries (where depth increased and decreased) were used to determine the position of the fasta sequence where the genome sequence was extracted using getFasta from **BEDtools**. 

We also created a blast database for the mitochondrial and chloroplast genomes, against which the extracted fasta sequences were blasted.

Lastly to look at repeat content, we used RepeatModeler and Repeatmasker to find repeats in the extracted fasta fiels. 
