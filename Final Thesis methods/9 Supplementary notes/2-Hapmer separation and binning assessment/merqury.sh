#!/bin/bash
#SBATCH --job-name=merq
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=250G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load canu/1.8

cd /home/FCAM/alotter/CBCNP/Canu_assembly/haplotype/0-kmers/
meryl greater-than 34 output mat.only.meryl ./haplotype-URO.meryl/
meryl greater-than 56 output pat.only.meryl ./haplotype-GRA.meryl/


#cd /home/FCAM/alotter/CBCNP/Canu_assembly/merqury/hapmers
cd /home/FCAM/alotter/CBCNP/Canu_assembly/merqury/triocanu_clr

ln -s ../../haplotype/0-kmers/mat.only.meryl/
ln -s ../../haplotype/0-kmers/pat.only.meryl/
ln -s ../../haplotype/0-kmers/haplotype-GRA.meryl/ paternal.meryl
ln -s ../../haplotype/0-kmers/haplotype-URO.meryl/ maternal.meryl

. ~/miniconda3/etc/profile.d/conda.sh
conda activate ~/miniconda3/envs/merqury/


sh $MERQURY/best_k.sh 650000000


# 2. Merge
#meryl union-sum output $genome.meryl read*.meryl

meryl union-sum maternal.meryl paternal.meryl output child.meryl
#sh $MERQURY/trio/hapmers.sh maternal.meryl paternal.meryl child.meryl

#conda update --all


#use inherited hapmers
sh ../merqury.sh child.meryl maternal.hapmer.meryl paternal.hapmer.meryl uro_unmasked.fasta gra_PS.fasta test1
Rscript $MERQURY/plot/plot_spectra_cn.R -f test1.spectra-cn.hist -o test1.spectra-cn
Rscript $MERQURY/plot/plot_spectra_asm.R -f test1.spectra-asm.hist -o test1.spectra-asm
Rscript $MERQURY/plot/plot_blob.R -f test1.hapmers.count -o test1.hapmers.blob
Rscript $MERQURY/plot/plot_block_N.R -b test1.gra_PS.100_20000.phased_block.sizes -s test1.gra_PS.scaff.sizes -c test1.gra_PS.contig.sizes -o test1.gra_PS.100_20000.phased_block
Rscript $MERQURY/plot/plot_block_N.R -b test1.uro_unmasked.100_20000.phased_block.sizes -s test1.uro_unmasked.scaff.sizes -c test1.uro_unmasked.contig.sizes -o test1.uro_unmasked.100_20000.phased_block

#test with lower switch error limit
sh $MERQURY/trio/switch_error.sh test1.uro_unmasked.sort.bed test1.uro 10 20000
sh $MERQURY/trio/switch_error.sh test1.gra_PS.sort.bed test1.gra 10 20000
sh $MERQURY/trio/block_n_stats.sh uro_unmasked.fasta test1.uro.10_20000.phased_block.bed gra_PS.fasta test1.gra.10_20000.phased_block.bed triocanu_clr.10_20000

