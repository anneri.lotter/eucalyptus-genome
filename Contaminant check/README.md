# Contaminat check and removal

This directory contains scripts related to identification and removal of reads that are not of the species of interest.

For **Illumina** reads, use **Kraken**, and for **Nanopore** reads use **Centrifuge**.

Running the **Kraken3.sh** script will only generate a contaminant report, whilst the **kraken_Illumina.sh** script will also create fastq files containing unclassified reads (which are the ones you would use in downstream assemblies) and classified reads (these are the contaminated reads which you will not want to use).

If the results from the default setting in Centrifuge (50 bp match) indicates a high contaminant level (**centrifuge.sh**), try rerunning with 100 bp minLength match (**centrifuge_euc.sh**), as 50 bp may be too short and may lead to false-positive classification due to error-prone Nanopore reads.

Note that scripts related to actual removal of reads identified by Centrifuge are in the **Hybrid_assembly_Canu_binned/Contaminant_QC** folder.

 
