# Base-calling and adapter removal of Nanopore reads

This directory contains scripts related to base-calling of **FAST5** reads produced from Nanopore sequencing. Reads can be base-called during sequencing or afterwards (as was done for the MinION runs). We used the **Guppy basecaller**, and would recommend using the GPU version as it is much faster (as specified in the base-calling scripts). Base-calling can also be performed again if one wishes to change parameters, but we used a standard QV>7 cutoff.

The resulting FASTQ files that pass QC (in the pass folder) or fail QC (fail folder) were concatenated into a single FASTQ file for each run.

Additionally, we used **PoreChop** to remove adapters from base-called reads, as the Guppy basecaller may have missed some.

Lastly all PoreChopped reads were concatenated ito a single FASTQ file for use in assemblies and checking for contaminants.
