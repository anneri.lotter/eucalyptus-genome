#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load BRAKER/2.0.5
module unload augustus/3.3.3   
module unload GeneMark-ET/4.59

export PATH=/home/FCAM/$USER/BRAKER:/home/FCAM/$USER/BRAKER/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/3.2.3/config
export TMPDIR=/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_gra2/CA.mr.41.15.15.0.02/polar_star/gra_allmaps/hisat2/braker/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin/
#the path to the locally installed BRAKER
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin/
export GENEMARK_PATH=/isg/shared/apps/GeneMark-ET/4.38/

cp ~/local_gm_key_64 ~/.gm_key
braker.pl --cores 20 --genome=grandis.fasta.masked --species=euc1grandis --bam all.bam  --softmasking 1 --gff3


