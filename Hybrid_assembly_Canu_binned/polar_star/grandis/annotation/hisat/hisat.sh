#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 19
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0

# building index
hisat2-build -p 19 grandis.fasta.masked grandis_index

# aligning with tunning for splice precision

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/flower/trim_CACT_fastq_forward.fastq -2 RNASeq/flower/trim_CACT_fastq_reverse.fastq -S MS10.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/flower/trim_GCTT_fastq_forward.fastq -2 RNASeq/flower/trim_GCTT_fastq_reverse.fastq -S MS11.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/flower/trim_TTGT_fastq_forward.fastq -2 RNASeq/flower/trim_TTGT_fastq_reverse.fastq -S MS12.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/roots/trim_GC03B-1.fq -2 RNASeq/roots/trim_GC03B-2.fq -S MS19.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/roots/trim_GC03_B2A-1.fq -2 RNASeq/roots/trim_GC03_B2A-2.fq -S MS20.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/roots/trim_GC05_BR1B-1.fq -2 RNASeq/roots/trim_GC05_BR1B-2.fq -S MS21.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/matureleaf/trim_CACT_fastq_forward.fastq -2 RNASeq/matureleaf/trim_CACT_fastq_reverse.fastq -S mlCACT.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/matureleaf/trim_GCTT_fastq_forward.fastq -2 RNASeq/matureleaf/trim_GCTT_fastq_reverse.fastq -S mlGCTT.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/phloem/trim_GCTT_fastq_forward.fastq -2 RNASeq/phloem/trim_GCTT_fastq_reverse.fastq -S phGCTT.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/phloem/trim_TTGT_fastq_forward.fastq -2 RNASeq/phloem/trim_TTGT_fastq_reverse.fastq -S phTTGT.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/shoot_tips/trim_CACT_fastq_forward.fastq -2 RNASeq/shoot_tips/trim_CACT_fastq_reverse.fastq -S stCACT.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/shoot_tips/trim_TTGT_fastq_forward.fastq -2 RNASeq/shoot_tips/trim_TTGT_fastq_reverse.fastq -S stTTGT.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/immature_xylem/trim_eu0004_4_1.qseq.fastq -2 RNASeq/immature_xylem/trim_eu0004_4_2.qseq.fastq -S ixeu0004.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/immature_xylem/trim_eu0006_6_1.qseq.fastq -2 RNASeq/immature_xylem/trim_eu0006_6_2.qseq.fastq -S ixeu0006.sam

hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/young_leaf/trim_eu0003_3_1.qseq.fastq -2 RNASeq/young_leaf/trim_eu0003_3_2.qseq.fastq -S yleu0003.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x grandis_index -1 RNASeq/young_leaf/trim_eu0005_5_1.qseq.fastq -2 RNASeq/young_leaf/trim_eu0005_5_2.qseq.fastq -S yleu0005.sam


module load samtools/1.9
# Convert HISAT2 sam files to bam files and sort by aligned position
samtools sort -@ 40 MS10.sam | samtools view -bS -F 0x04 - > MS10.OnlyAligned.bam
samtools sort -@ 40 MS11.sam | samtools view -bS -F 0x04 - > MS11.OnlyAligned.bam
samtools sort -@ 40 MS12.sam | samtools view -bS -F 0x04 - > MS12.OnlyAligned.bam
samtools sort -@ 40 MS19.sam | samtools view -bS -F 0x04 - > MS19.OnlyAligned.bam
samtools sort -@ 40 MS20.sam | samtools view -bS -F 0x04 - > MS20.OnlyAligned.bam
samtools sort -@ 40 MS21.sam | samtools view -bS -F 0x04 - > MS21.OnlyAligned.bam
samtools sort -@ 40 mlCACT.sam | samtools view -bS -F 0x04 - > mlCACT.OnlyAligned.bam
samtools sort -@ 40 mlGCTT.sam | samtools view -bS -F 0x04 - > mlGCTT.OnlyAligned.bam
samtools sort -@ 40 phGCTT.sam | samtools view -bS -F 0x04 - > phGCTT.OnlyAligned.bam
samtools sort -@ 40 phTTGT.sam | samtools view -bS -F 0x04 - > phTTGT.OnlyAligned.bam
samtools sort -@ 40 stCACT.sam | samtools view -bS -F 0x04 - > stCACT.OnlyAligned.bam
samtools sort -@ 40 stTTGT.sam | samtools view -bS -F 0x04 - > stTTGTOnlyAligned.bam
samtools sort -@ 40 ixeu0004.sam | samtools view -bS -F 0x04 - > ixeu0004..OnlyAligned.bam
samtools sort -@ 40 ixeu0006.sam | samtools view -bS -F 0x04 - > ixeu0006.OnlyAligned.bam
samtools sort -@ 40 yleu0003.sam | samtools view -bS -F 0x04 - > yleu0003.OnlyAligned.bam
samtools sort -@ 40 yleu0005.sam | samtools view -bS -F 0x04 - > yleu0005.OnlyAligned.bam

module load samtools/1.9
samtools merge -nurlf -b bamlist.fofn all.bam
