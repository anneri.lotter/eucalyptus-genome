#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=60G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load Trimmomatic/0.39
java -jar $Trimmomatic PE -threads 8 -summary CACT.txt -validatePairs CACT_fastq_forward.fastq CACT_fastq_reverse.fastq \
	trim_CACT_fastq_forward.fastq singles_trim_CACT_fastq_forward.fastq \
	trim_CACT_fastq_reverse.fastq singles_trim_CACT_fastq_reverse.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary TTGT.txt -validatePairs TTGT_fastq_forward.fastq TTGT_fastq_reverse.fastq \
	trim_TTGT_fastq_forward.fastq singles_trim_TTGT_fastq_forward.fastq \
	trim_TTGT_fastq_reverse.fastq singles_trim_TTGT_fastq_reverse.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30
