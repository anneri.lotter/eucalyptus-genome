#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=60G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load Trimmomatic/0.39
java -jar $Trimmomatic PE -threads 8 -summary GC03Bsum.txt -validatePairs GC03B-1.fq GC03B-2.fq \
	trim_GC03B-1.fq singles_trim_GC03B-1.fq \
	trim_GC03B-2.fq singles_trim_GC03B-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GC03_B2Asum.txt -validatePairs GC03_B2A-1.fq GC03_B2A-2.fq \
	trim_GC03_B2A-1.fq singles_trim_GC03_B2A-1.fq \
	trim_GC03_B2A-2.fq singles_trim_GC03_B2A-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GC05_BR1Bsum.txt -validatePairs GC05_BR1B-1.fq GC05_BR1B-2.fq \
	trim_GC05_BR1B-1.fq singles_trim_GC05_BR1B-1.fq \
	trim_GC05_BR1B-2.fq singles_trim_GC05_BR1B-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

