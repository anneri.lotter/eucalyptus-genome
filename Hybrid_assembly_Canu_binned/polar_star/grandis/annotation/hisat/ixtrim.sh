#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=60G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load Trimmomatic/0.39
java -jar $Trimmomatic PE -threads 8 -summary eu0004.txt -validatePairs eu0004_4_1.qseq.fastq eu0004_4_2.qseq.fastq \
	trim_eu0004_4_1.qseq.fastq singles_trim_eu0004_4_1.qseq.fastq \
	trim_eu0004_4_2.qseq.fastq singles_trim_eu0004_4_2.qseq.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -phred64 -threads 8 -summary eu0006.txt -validatePairs eu0006_6_1.qseq.fastq eu0006_6_2.qseq.fastq \
	trim_eu0006_6_1.qseq.fastq singles_trim_eu0006_6_1.qseq.fastq \
	trim_eu0006_6_2.qseq.fastq singles_trim_eu0006_6_2.qseq.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

