#!/bin/bash
#SBATCH --job-name=rpm
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem2 
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=350G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o repeatmasker-%j.output 
#SBATCH -e repeatmasker-%j.error 

# Run the program                 
echo GU2.fasta

module load RepeatMasker/4.0.6 
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

# The pipe "|" here creates an output text file that can be checked for errors
RepeatMasker GU2.fasta -dir /scratch/alotter/repeatmodeler/grandis/RM_23268.SunMay242311192020/repeatmasker_out_clas -lib /scratch/alotter/repeatmodeler/grandis/RM_23268.SunMay242311192020/consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall| tee GU2.txt
