#!/bin/bash
#SBATCH --job-name=quast_uro
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load quast/5.0.2
#quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/polar_star/uro_masurca.new_fasta.fasta -o quast_uro

quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/polar_star/3kb_uro_masurca_new.fasta -o quast_uro3kb
	
