#!/bin/bash
#SBATCH --job-name=bwa
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load bwa/0.7.5a
#bwa index ../GU2.fasta
bwa mem -t 8 ../GU2.fasta /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_1.fastq /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_2.fastq > gratouro_map_nanopore.sam

module load samtools/1.9
samtools view -S -b gratouro_map_nanopore.sam > gratouro_map_nanopore.bam
samtools index -b gratouro_map_nanopore.bam
samtools flagstat gratouro_map_nanopore.bam > statsgratouro.out
