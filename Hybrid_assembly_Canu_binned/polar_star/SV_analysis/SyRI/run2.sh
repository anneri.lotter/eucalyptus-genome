#!/bin/bash
#SBATCH --job-name=syri
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=350G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

. ~/miniconda3/etc/profile.d/conda.sh
#conda activate ~/miniconda3/envs/py35_new/
conda activate ~/miniconda3/envs/py35/
cwd="."     # Change to working directory
PATH_TO_SYRI="./syri/bin/syri" #Change the path to point to syri executable
PATH_TO_PLOTSR="./syri/bin/plotsr" #Change the path to point to plotsr executable


# Using minimap2 for generating alignment. Any other whole genome alignment tool can also be used.
minimap2 -ax asm5 --eqx gra_PS_split_AM.chr.fasta uro.PS_AM_unmasked.chr.fasta > grarescafvsurohap.sam

samtools view -b grarescafvsurohap.sam > grarescafvsurohap.bam 

python3 $PATH_TO_SYRI -c grarescafvsurohap.sam ./gra_PS_split_AM.chr.fasta ./uro.PS_AM_unmasked.chr.fasta -k -F B --no-chrmatch

# Using SyRI to identify genomic rearrangements from whole-genome alignments generated using MUMmer. A .tsv (out.filtered.coords) file is used as the input.
# Whole genome alignment. Any other alignment can also be used.
nucmer --maxmatch -c 100 -b 500 -l 50 ./gra_PS_split_AM.chr.fasta ./uro.PS_AM_unmasked.chr.fasta -p grarescafvsurohap

# Remove small and lower quality alignments
delta-filter -m -i 90 -l 100 grarescafvsurohap.delta > grarescafvsurohap.filtered.delta
# Convert alignment information to a .TSV format as required by SyRI
show-coords -THrd grarescafvsurohap.filtered.delta > grarescafvsurohap.filtered.coords

python3 $PATH_TO_SYRI -c grarescafvsurohap.filtered.coords -d grarescafvsurohap.filtered.delta -r ./gra_PS_split_AM.chr.fasta -q ./uro.PS_AM_unmasked.chr.fasta -k --no-chrmatch


# Plot genomic structure
python3 $PATH_TO_PLOTSR syri.out ./gra_PS_split_AM.chr.fasta ./uro.PS_AM_unmasked.chr.fasta -o pdf -d 600

show-coords -THrd grarescafvsurohap.delta > grarescafvsurohap.coords

nucmer --maxmatch -l 100 -c 500 ./gra_PS_split_AM.chr.fasta ./uro.PS_AM_unmasked.chr.fasta -prefix asb_grarescafvsurohap

