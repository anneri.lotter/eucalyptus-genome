#!/bin/bash
#SBATCH --job-name=UroBintoRef
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
minimap2 -ax asm20 /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/AllChr_mt_ch.fasta /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/alignments/UROBintoGraref.sam

module load samtools/1.9
samtools view -S -b UROBintoGraref.sam > UROBintoGraref.bam
samtools sort UROBintoGraref.bam -o UROBintoGraref.sorted.bam
samtools index UROBintoGraref.sorted.bam
samtools depth UROBintoGraref.sorted.bam > UroBintoGraref.txt
samtools depth UROBintoGraref.sorted.bam | awk '{sum+=$3} END { print "Average = ",sum/NR}' > total_UroBintoGraref.txt

