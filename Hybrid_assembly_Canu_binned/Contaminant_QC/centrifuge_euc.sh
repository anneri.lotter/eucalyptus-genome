#!/bin/bash
#SBATCH --job-name=centrifuge_euc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
​
module load centrifuge/1.0.4-beta
​
centrifuge -x p+h+v --report-file centrifuge_euc_GRA_report.tsv --quiet --min-hitlen 100 -f /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/haplotype-GRA.fasta
centrifuge -x p+h+v --report-file centrifuge_euc_unclassified_report.tsv --quiet --min-hitlen 100 -f /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/haplotype-unknown.fasta
