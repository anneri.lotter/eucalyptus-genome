#!/bin/bash
#SBATCH --job-name=masurca
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/canu_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/canu_%j.err

# canu fucntion
module load canu/1.8
canu \
	-p Canu_AllData_1000 -d /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/ genomeSize=650m \
	-minReadLength=1000 \
	-useGrid=True \
	-haplotypeURO /projects/EBP/CBC/eucalyptus/rawReads/Illumina/ \
	-haplotypeGRA /projects/EBP/CBC/eucalyptus/rawReads/Illumina/ \
        -nanopore-raw /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/

