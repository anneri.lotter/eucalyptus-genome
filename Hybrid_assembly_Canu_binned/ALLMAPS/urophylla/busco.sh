#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

#module load busco/3.0.2b

module load busco/4.0.2
module unload augustus
export PATH=/home/FCAM/alotter/augustus-3.2.3/3.2.3/bin:/home/FCAM/alotter/augustus-3.2.3/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/3.2.3/config
###if your run crashes uncomment the following:
#module unload blast/2.7.1
#module load blast/2.2.29 

busco -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/Allmaps/Uro_test/Maps2.fasta -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o buscoV4.0_eucalyptus_annotation -m geno -c 8  
busco -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/Allmaps/Uro_test/Maps2.chr.fasta -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o busco_eucalyptuschr_annotation -m geno -c 8
