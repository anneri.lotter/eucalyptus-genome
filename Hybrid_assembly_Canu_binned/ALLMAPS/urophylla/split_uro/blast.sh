#!/bin/bash
#SBATCH --job-name=blast_uro
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load blast/2.7.1
makeblastdb -in genome.SPLIT.fasta -dbtype nucl -out Uro_db
blastn -num_threads 16 -query matched_uro1.fasta -db Uro_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out Uro_SNP_Genome.blastp
blastn -num_threads 16 -query matched_gra1.fasta -db Uro_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out Gra_SNP_Genome.blastp

