#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load quast/5.0.2
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/Allmaps/Gra_mapped/gra_map_only/Gra.fasta -o quast_gra
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/Allmaps/Gra_mapped/gra_map_only/Gra.chr.fasta -o quast_gra_chr

