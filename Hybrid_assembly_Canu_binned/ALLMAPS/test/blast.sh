#!/bin/bash
#SBATCH --job-name=blast_uro
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load blast/2.7.1
#makeblastdb -in final.genome.scf.fasta -dbtype nucl -out Uro_db
blastn -num_threads 16 -query matched_uro1.fasta -db /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/Uro_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out SNP_Genome.blastp
#blastn -num_threads 16 -query matched_uro1.fasta -db /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/Uro_db -evalue 0.00001 -outfmt 17 -num_alignments 1 -max_hsps 1 -out SNP_Genome.sam
blastn -num_threads 16 -query matched_uro1.fasta -db /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/Uro_db -evalue 0.00001 -outfmt “6 qstart qend sstart send”  -num_alignments 1 -max_hsps 1 -out SNP_Genome1.blastp
