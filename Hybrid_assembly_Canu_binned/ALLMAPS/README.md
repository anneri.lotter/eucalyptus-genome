# Genome scaffolding with ALLMAPS

To use **ALLMAPS** the following steps were followed:

### 1) Create a file containing SNP names and probes with awk

*       awk '{print $1"\t"$2}' SNP Probes.txt > snp_probe_name.txt

### 2) Create file with SNP probes and SNP names corresponding to SNP names in the genetic linkage map

*      
*

### 3) Convert probe files to .fasta files for use in BLAST

*       awk '{ printf ">%s\n%s\n",$1,$2 }' matched_uro1.txt > matched_uro1.fasta
*       awk '{ printf ">%s\n%s\n",$1,$2 }' matched_gra1.txt > matched_gra1.fasta

### 4) Create **BLAST** database for the genome assembly and BLAST probes to assembly to find position

**Input:**

*	Draft genome assembly in FASTA format
*	**blast.sh** script (in relevant directory)

**Output:**

*	Database to blast against, consisting of your assembled genome
*	**.blastp** file containing the following info/headers: qseqid (query seq), sseqid (ref genome), pident (% identity match), length (of alignment), mismatch (number mismatches), gapopen (number gap openings), qstart (query alignment start), qend (query alignment end), sstart (ref alignment start), send (ref alignment end), evalue, bitscore

### 5) Create **.csv** file for running ALLMAPS (use blastp file)

*       awk '{print $1"\t"$2"\t"$9"\t"$10 }' SNP_Genome.blastp > UroSNP_Probes_to_Assembly.txt
*       awk '{print $3"\t"$1":"$4}' urophylla_map_SNPS.txt > urophylla_map_SNPS_map.txt
*       awk 'BEGIN { FS = OFS = "\t" } NR == FNR {fn[$1] = $2; next} {print $2, $3, ($1 in fn ? fn[$1] : "FALSE")}' urophylla_map_SNPS_map.txt UroSNP_Probes_to_Assembly.txt | sed 's/%/\t/' > AllMap1.file
*       tr '\t' ',' < AllMap1.file > UroAllMap1.final.csv

Note that I used the SNP probe start position and not the start to end position.

### 6) Using a text editor, insert the headers as specified on the ALLMAPS github page.

In this case we specify Scaffold ID,scaffold position,LG,genetic position.

### 7) Run ALLMAPS by running the **run.sh** script

**Input:**

*	**.csv** file of genetic linkage maps (preferably both parents as this increase scaffolding rate of contigs)
*	**run.sh** script to run scaffolding
*	Draft genome assembly file in FASTA format

**Output:**

*	**.bed** file containing the SNP positions for both genetic linkage maps
*	**weight.txt** file containing the weight each genetic map will be given (in this case this will be adjusted depending on the haplotype being assembled so the parental haplotype carries greater weight i.e. pivot map)
*	FASTA file woth reconstructed chromosome sequences
*	PDF files with alignment visualisations corresponding to reconstructed chromosomes
*	**summary.txt** file containing info regarding number of scaffold anchored, N50 etc.

You can also estimate gap lengths and redo the assembly.

### 8) Run QUAST and BUSCO to estimate genome assembly statistics after scaffolding.

**ALLMAPS** also outputs pdf images of congruence between physical and genetic maps. If a scaffold contains areas with multiple markers mapping to another linkage group, the genome can be split using the [split function](https://github.com/tanghaibao/jcvi/wiki/ALLMAPS:-How-to-split-chimeric-contigs).

### 9) Improve contig anchoring

ALLMAPS allows the user to specify splitting of contigs in the assembly. In this case we specify breakpoints using the genetic maps where or more markers mapped to a different linkage group (i.e. markers from LG11 mapped to a contig that was anchored on chromosome4).

**Input:**

*	Original genome assembly file (final.genome.scf.fasta) in FASTA format
*	**.bed** integrated genetic linkage map (generated previously) 
*	**run.sh** script to run genome splitting and scaffolding. **Note:** The **--chunks** parameter specifies how many markers may match another LG. Run from line 31 to 45.

**Output:**

*	**breakpoint.bed** file specifying breakpoint positions
*	**genome.gap.bed** file specifying breakpoint position in  a more refined manner. It works by finding the largest gap in the breakpoint region
*	**genome.fasta.agp** file specifying how the genome should be split
*	**genome.SPLIT.fasta** file containing the split genome in FASTA format. This would be used and should contain the original number of contigs plus the amount of breakpoints as a total number of contigs. The total size should however still be the same as in the final.genome.scf.fasta file.

After the genome has been split, marker mapping should be rerun using the split genome (we used blast to find SNP probe positions). After **.csv** files are created for the new positions, line 47 can be run. Weight adjustment may be needed again, after which scaffolding can be performed (line 49).

Quality of the anchored genome can again be assessed using BUSCO and QUAST.

All scripts related to genome splitting and scaffolding cen be found in the **grandis/split_gra** and the **urophylle/split_uro** directories.
