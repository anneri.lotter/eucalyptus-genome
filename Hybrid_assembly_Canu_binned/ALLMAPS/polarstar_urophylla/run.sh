#!/bin/bash
#SBATCH --job-name=Allmaps_Gra
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load ALLMAPS/144a067

# Prepare ALLMAPS input
#python -m jcvi.assembly.allmaps merge gra_allmap.csv uro_allmap.csv -o GU2.bed -w weights.txt

# Run ALLMAPS
python -m jcvi.assembly.allmaps path GU2.bed 3kb_uro_masurca_new.fasta -w weights.txt

# Estimate gap lengths
#python -m jcvi.assembly.allmaps estimategaps UroAllMap1.bed

# Split contigs with 4 or more markers matching to multiple linkage groups
#python -m jcvi.assembly.allmaps splitGU2.bed --chunk=4 > breakpoints.bed

# Find gaps in assembly 
#python -m jcvi.formats.fasta gaps GU2.fasta

# Refine breakpoint by finding largest gaps in breakpoint
#python -m jcvi.assembly.patch refine breakpoints.bed GU2.gaps.bed

