#!/bin/bash
#SBATCH --job-name=quast_gra
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load quast/5.0.2
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_gra2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -o quast_gra

