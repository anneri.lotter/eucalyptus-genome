# Assembly of Canu trio-binned reads

This directory contains scripts related to assembly of reads that were trio-binned using the Canu genome assembly tool.

## **1) Trio-binning**

The Canu_correct_minread1000 script can be used to perform Trio-binning (this is the first stage in the Canu assembler process).

For this step one requires Illumina sequencing reads for both parental haplotypes (which is specified with the haplotype function) as well as long-read sequencing data of the F1 hybrid offspring (put in as nanopore-raw)

Altough the binning step cannot be isolated like with correction, trimming and assembly steps, it is the first step and will result in three fasta files to be generated in the haplotype folder of the Canu assembly directory specified in your script.

Please refer to this website for help with Canu https://canu.readthedocs.io/en/latest/quick-start.html

## **2) Contaminant removal from Nanopore reads**

If removal of contaminated reads has not yet been performed, this would be the time to do so, before assembly.

First, run **Centrifuge** to remove identify contaminated reads. THis wil generate a tsv file, which can be used with the isolate_contaminated_read_ids python script to generate a list of contaminated reads.

After the read list has been made, reads that are contaminated can be removed from the haplotype-fasta files (or this can be done before binninguing the fastq files and just changing that parameter in the remove_contaminated_reads script)

Hereafter only reads that are uncontaminated (this include unclassified Illumina reads) will be used for assembly

## **3) Hybrid Masurca genome assembly**

Genome assembly was performed with Masurca, as it seems to deliver the best results i.t.o. number of contigs and contiguity (see results of Masurca hybrid assembly). Since only long-reads were binned, Illumina reads of the parents were used in this assembly (the reads corresponding to the specified haplotype were used). This assembler was also chosen because it is faster than Canu.

Scripts for the assemblies are in the gra (for *E. grandis*) and uro (for *E. urophylla*) folders, along with genome assembly quality assessment scripts.

## **4) Checking genome size**

To check if the assembled genome size is correct (and that the reduced assembly size compared to the estimated size is not due to reads not being assembled), **bwa mem** can be used along with the **flagstat** function from **samtools** to check alignment of raw Illumina reads to the assembled genome. If the assembly is correct and no elements are not being assembled, you would expect a mapping rate of **99%**.

**Input:**
*	**bwa_align.sh** script to run bwa mem and calculate alignment statistics

**Output:**
*	**.sam** and **.bam** alignments files containg raw Illumina reads aligned to the assembled genome
*	**stats.out** file containing the alignment statistics. If all the reads were used a 99% alignment score is expected.

## **5) Inferring breakpoints**

After looking at results from ALLMAPS, it was found that there may be chimeric contigs after assembly with MaSuRCa. In addition, since we only used about 3,000 SNP markers in the high density genetic linkage maps, inferring contig breakpoints with them will not allow identification of the exact breakpoint position. A better way in which to infer breakpoint positions is based on read-depth evidence and then identification of outliers. This is commonly used when scaffolding with Hi-C reads, but we can also use raw binned ONT reads and **Polar Star**. 

Scripts for Polar_Star are in the polar_star folder.

**Input:**
*   **config.json** file specifying the location of the binned reads and the genome to be validated
*   **polarstar.sh** script to run polar_star
*   Need to set up a conda evironment to for polar_star to run in as specified on their [website](https://github.com/phasegenomics/polar_star/blob/master/README.md)

**Output:**
*   **new_fasta.fa** containing split contigs

Note: All read of 3 Kb were removed before genome scaffolding. One can do this either way described below with seqtk or the seqkit tool.
    seqkit seq -m 3000 your_fasta.fa > 3Kb.fasta
    seqtk seq -L 3000 contigs.fasta > file.fasta            

To check if scaffolds were assembled correctly, one would align raw reads to the scaffolded assembly and check for errors via IGV

## **6) Genome scaffolding**

After genome assembly with Masurca, the genome can be scaffolded using high-density genetic linkage maps using the **ALLMAPS** program. 

To do so the following is required to generate input files for ALLMAPS:

1) **BLAST** the SNP probe sequences against the assembled genome to find their genetic position.

2) Generate a .csv file containing the scaffold ID, scaffold position (this will correspons to the SNP position on the scaffold), the LG (linkage group to which the scaffold belongs, from the genetic linkage map) and the position (physical position as in the gentic map).

This process is better explained in the **ALLMAPS** directory. The file requirements are also specified [here](https://github.com/tanghaibao/jcvi/wiki/ALLMAPS).

3) Run the ALLMAPS script.

## **7) Repeat Masking**

After scaffolding of the genome and all alignments have been checked, repetitive content of the genome must be identified, annotated and softmasked before the genome can be used for annotation or SV analysis.

Here I used RepeatModeler to create a identify repeat elements and create a library of the repeat content for masking with RepeatMasker.

*RepeatModeler*

**Input:**
*   **FASTA** file of the scaffolded assembly (including the unplaced contigs)
*   **repeatmodeler.sh** script that can be found in the repeat_masking folders for each species assembly

**Output:**
*   **consensi.fa.classified** file containing all the classified repetitive elements
*   **consensi.fa** file withh all repetitive elements (including unknown) to use in RepeatMasker

*RepeatMasker*

**Input:**
*   **FASTA** file of scaffolded assembly (again including unplaced contigs)
*   **consensi.fa** file generated with RepeatModeler

**Output:**
*   **fasta.masked** file containing softmasked genome
*   **.align** file containing repeat alignments
*   **.tbl** file containing summary of each class of element and its prevalence in the genome

## **8) SV analysis**

This section is focused on sequence based SV detection and characterization. I made use of two different pipelines (both require their own conda environment to run in, speciefied in the links) namely [SyRI](https://schneebergerlab.github.io/syri/) and [ont-pipeline sv](https://github.com/nanoporetech/ont_tutorial_sv).
