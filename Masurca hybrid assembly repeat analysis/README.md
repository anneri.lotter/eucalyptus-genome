# Masking of repeat elements in the assembled genome

## **1) RepeatModeler**

The first step in annotation is to softmask the genome. This finds repeated elements in the genome and changes those nucleotides to lowercase letters (i.e. ATCG to atcg). Some of the analysis tools down the line consider this information. 

To begin the masking process, we must first use *RepeatModeler* on the filtered data.

Identify the fasta file on line 18 of repeatmodeler.sh, and the correct directory on line 17. Title the database on line 19 to keep the process organized. 

*RepeatModeler* will output a set of `consensi` files, which are used in *RepeatMasker*.

**Input:**

*	**repeatmodeler.sh** script to run repeatmodeler
*	Assembled genome file in FASTA format

**Output:**

*	A **consensi.fa** file containing repeat element database for masking


## **2) Split the genome**

Once we have the consensi files prepared, we want to split up our genome so that the masking process can procede more quickly. We use `splitfasta.py` in order to do so.  

Follow the format `python splitfasta.py --fasta [genome fasta] --path [genome path] --pieces [I chose 500 but this may vary by genome size and resources] --pathOut [output path]`. We loaded in dependencies using Anaconda2 (v 4.4.0), which is installed as a module in our cluster. Anaconda is easy to install locally if it's not available as a module in your system. splitfasta.py is relatively fast, so it can either be wrapped in a script or run on command line.

**Input:**

*	**splitfasta.py** script to split the genome
*	Assembled genome you want to split in FASTA format

**Output:**

*	Multiple FASTA files (split genome files)


## **3) RepeatMasker**

With your genome torn to pieces, you can then create an array job on slurm to mask all 500 at once. Depending on the number of pieces you chose to split your genome into, use that for line 14 in `repeatmasker.sh`. You will also adjust the `$SLURM_ARRAY_TASK_ID` field so that, when used in your input for the RepeatMasker script, it matches the form of your split genome files.

**Input:**

*	Assembled genome in multiple pieces in FASTA format
*	**repeatmasker.sh** script to mask the assembled genomic pieces

**Output:**

*	Masked genome pieces
*	Summary of masking percentage per genome segment


## **4) Reconstruction of the genome file**

After this has finished, and it should take a bit, you can simply `cat` your pieces back together and examine your masked genome for quality control. Use a wildcard (\*) to indicate where the numbers are located. If I wanted to print all softmasked genome pieces, I would use `cat IronWalnut*.fa > IronWalnut.output.fa`, where the `>` indicates where I want to write the new, combined file.

**Input:**

*	Masked genome assembly FASTA files

**Output:**
	
*	A single masked genome assembly in FASTA format


## **5) QC**

An important quality control check at the end of this step is to ensure that areas in the output fasta file have been changed to `X`s or lowercase letters. While repeated content is variable across genomes, a good check can be checking for the number of lowercase letters using a `vim` command: `:s/atcg//gn`, and comparing that to the number of characters in the fasta file `wc -c YourFasta.fasta`. This can give you a rough percentage, and one would expect percantages to range from maybe 10% to 40%, with exceptions. It is wise to make sure that each genome piece has been softmasked.

A very low percentages (<1%) might indicate that repeatModeler may not have been completed properly.  
  


