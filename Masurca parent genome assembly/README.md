# Independant short-read assembly of parental genomes

The genomes of the *E. urophylla* and *E. grandis* parents were assembled independantly with the **Masurca** genome assembly using only Illumina short reads.

Scripts related to the assembly as well as assessment of assembly quality can be found in the respective species directories.

**Input:**

*	**masurca_ONT.sh** script specifying computational parameters for the assembly
*	**config.txt** file specifying assembly parameters (note only Illumina reads are used)
*	Raw Illumina short-reads in FASTQ format

**Output:**

*	Draft genome assembly
*	**assemble.sh** script to run the assembly

The next step is to run **BUSCO** and **QUAST** to assess assembly quality.

**Note:** The resulting genome assemblies may contain contigs that are shorter than 1000 bp. As such I used the following command to remove all contigs shorter than 3000 bp:

*      seqkit seq -m 3000 your_fasta.fa > out.fasta

The resulting file was again assessed with BUSCO and QUAST to assess the effect of removal of short contigs (in this case the effect was not substantial - refer to spreadsheet on home page).

