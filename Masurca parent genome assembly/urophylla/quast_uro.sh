#!/bin/bash
#SBATCH --job-name=quast_par
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/CA/%x_%A.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/CA/%x_%A.err

module load quast/5.0.2
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/CA/final.genome.scf.fasta -o Masurca_uro_quast

quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/CA/genome_scf_3kb.fasta -o Masurca_uro_quast_3kb

