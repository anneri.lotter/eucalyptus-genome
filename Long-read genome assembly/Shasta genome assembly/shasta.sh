#!/bin/bash
#SBATCH --job-name=shasta_assembly_long_reads
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=450G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Shasta1/%x_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Shasta1/%x_%j.err

module load shasta/0.2.0
#To convert fastq file to fasta for shasta assembly
paste - - - - < GUF1_PoreChopped_MinIONandPromethION.fastq | cut -f 1,2 | sed 's/^@/>/' | tr "\t" "\n" > GUF1_PoreChopped_MinIONandPromethION.fasta

shasta --input /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fasta --assemblyDirectory /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Shasta2 --Reads.minReadLength 500 --memoryMode anonymous --memoryBacking 4K --threads 16
