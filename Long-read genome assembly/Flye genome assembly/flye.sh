#!/bin/bash
#SBATCH --job-name=flye_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/%x_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/%x_%j.err
module load flye/2.4.2
flye --nano-raw /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fastq \
 --genome-size 650m --out-dir /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/Flye_Rawreads_ONT --threads 36
