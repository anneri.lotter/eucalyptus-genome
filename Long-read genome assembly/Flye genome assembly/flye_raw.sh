#!/bin/bash
#PBS -l nodes=1:ppn=24
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o /nlustre/users/anneri/CombinedONT/flye/flye_stderr.log
#PBS -e /nlustre/users/anneri/CombinedONT/flye/flye_stdout.log
#PBS -k oe
#PBS -m ae
#PBS -M anneri.lotter@fabi.up.ac.za

module load flye
flye --nano-raw /nlustre/users/anneri/CombinedONT/GUF1_PoreChopped_MinIONandPromethION.fastq \
        --genome-size 650m -o /nlustre/users/anneri/CombinedONT/flye -t 24