# Long-read based genome assemblies

This directory contains scripts related to assembly of the F1 hybrid genome using only Nanopore reads. All scripts relating to genome assembly quality analysis can also be found here as well as scripts related to any further analysis performed on a particular assembly (such as removal of redundant reads). Links to all results from quality assessment can be found in the root directory.

The following assemblers were tested:

*	Canu

*	FALCON

*	Flye

*	SHASTA

*	wtdbg2

All of the assemblies were run without any polishing of Nanopore reads. The bottom four assembler are relatively quick (taking a 12 hours up to 5 days to complete).


## **CANU**

As Canu is a slow genome assembler, and we opted to only use it only for trio-binning (see **Hybrid_assembly_Canu_binned** folder).

Please refer to the [CANU guide](https://canu.readthedocs.io/en/latest/quick-start.html) for all information on parameters and help.

**Input:**

*	**Canu.sh** script to specify assembly parameters and run assembly
*	Raw Illumina short-reads of both parents in FASTQ format (parental bins must be specified)
*	Raw ONT long-reads in FASTQ format

**Output (binning only):**

*	A haplotype folder, containing three FASTA files. Two correspond to the two parental read bins, and one unknown bin file. We will use the two binned read files for genome assembly. 

**Note:** CANU automatically adds unknown bin-reads to both haplotypes if they represent more than 5% of the FASTQ reads.

The binned reads can be found in the following directory:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/haplotype

## **FALCON**

Note that FALCON is meant for PacBio sequencing data, and as such we had to change the Nanopore FASTA file to be compatible with the FALCON assembler. To do so we used a script to fake PacBio headers (**change-names.py**) and used the resulting FASTA file for assembly with FALCON. This script is fast and can be run using an interactive session.

Please refer to the [FALCON github page](https://github.com/PacificBiosciences/FALCON/wiki/Manual) regarding environment setup and parameter specifications.

**Input:**

*	**change_names.py** script to fake PacBio FASTA headers
*	**falcon_add.sh** script to activate the environment for FALCON to run in
*	**fc_run.py** script to tun FALCON assembly 
*	**subreads.fasta.fofn** file specifying the path to the FASTA file(/s)

**Output:**

*	Assembled genome (likely containing redundant reads)

The draft assembly can be found in the following directory:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Falcon/2-asm-falcon

We also used **PurgeHaplotigs** to remove redundant reads from the FALCON based assembly, as the FALCON-Unzip module is not compatible with Nanopore sequencing data.


## **Flye**

**Input:**

*	**flye.sh** script for genome assembly
*	Raw ONT long reads in FASTQ format

**Output:**

* Draft long-read genome assembly

The draft assembly can be found in the following directory:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/haplotype

Please refer to the [Flye github page](https://github.com/fenderglass/Flye) for information on parameter choices.


## **SHASTA**

Note that SHASTA requires a FASTA file as input, which can be generated using the command on line 16 of the **shasta.sh** script.

**Input:**

*	Raw ONT long reads in FASTA format
*	**shasta.sh** script to run the assembly

**Output:**

*	Draft long-read genome assembly 

The draft assembly can be found in the following directory:

*       /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Shasta2

Please refer to the [SHASTA github page](https://chanzuckerberg.github.io/shasta/) for help with parameter choices.


## **WTDBG2**

**Input:**

*	Raw ONT long reads in FASTQ format
*	**wtdbg2_corrected.sh** script to run genome assembly

**Output:**

*	Draft long-read genome assembly

Please refer to the [wtdbg2 github page](https://github.com/ruanjue/wtdbg2) for further information regarding thw assembler.




