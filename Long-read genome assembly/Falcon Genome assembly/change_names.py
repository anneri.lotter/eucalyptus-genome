import sys
import os
## This script takes non-pacbio fasta data and fakes the headers, for input to FALCON
def change_headers(fasta):
  count = 1
  basename=os.path.basename(fasta)
  suffix_loc = basename.find(".")
  name = basename[:suffix_loc]
  newname = name+"-changed.fasta"
  print("New file: %s" % newname)
  newf = open(newname,'w')
  with open(fasta,'r') as rf:
    line = rf.readline()
    while(line):
      if(line.startswith("#")):
        line = rf.readline()
        continue
      assert(line.startswith(">"))
      seqline = rf.readline()
      newf.write(">name/%d/0_%d\n" % (count,len(seqline)-1))
      newf.write(seqline)
      count+=1
      line = rf.readline()
  newf.close()
if(__name__=="__main__"):
  try:
    fasta_name = sys.argv[1]
  except IndexError:
    print("Usage: python change_headers_falcon.py <path_to_fasta_file>")
    sys.exit()
  change_headers(fasta_name)
