#!/bin/bash
#SBATCH --job-name=blastdb
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load blast/2.7.1

# make protein blast database
makeblastdb -in eg.faa -dbtype prot -parse_seqids -out eg
makeblastdb -in eu.faa -dbtype prot -parse_seqids -out eu
#makeblastdb -in euc.faa -dbtype prot -parse_seqids -out euc


blastp -query eg.faa -db eg -out egr.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
blastp -query eu.faa -db eu -out eur.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
#blastp -query euc.faa -db euc -out euc.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
#blastp -query egr.faa -db euc -out egr_euc.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
#blastp -query eur.faa -db euc -out eur_euc.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5

blastp -query eu.faa -db eg -out eg_eu.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
blastp -query eg.faa -db eu -out eu_eg.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
