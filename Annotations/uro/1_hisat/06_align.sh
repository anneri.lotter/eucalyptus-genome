#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 19
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0

# building index
hisat2-build -s -p 19 ../uro_final_masked.fasta urophylla_index

# aligning with tunning for splice precision
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M546_1.fq -2 ~/GUxU3yo/trim_M546_2.fq -S M546.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M640-1.fq -2 ~/GUxU3yo/trim_M640-2.fq -S M640.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M645-1.fq -2 ~/GUxU3yo/trim_M645-2.fq -S M645.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M648-1.fq -2 ~/GUxU3yo/trim_M648-2.fq -S M648.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M656-1.fq -2 ~/GUxU3yo/trim_M656-2.fq -S M656.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M658-1.fq -2 ~/GUxU3yo/trim_M658-2.fq -S M658.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M661-1.fq -2 ~/GUxU3yo/trim_M661-2.fq -S M661.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M668-1.fq -2 ~/GUxU3yo/trim_M668-2.fq -S M668.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M723-1.fq -2 ~/GUxU3yo/trim_M723-2.fq -S M723.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M728-1.fq -2 ~/GUxU3yo/trim_M728-2.fq -S M728.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M809-1.fq -2 ~/GUxU3yo/trim_M809-2.fq -S M809.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M826-1.fq -2 ~/GUxU3yo/trim_M826-2.fq -S M826.sam 
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 ~/GUxU3yo/trim_M801-1.fq -2 ~/GUxU3yo/trim_M801-2.fq -S M801.sam

module load samtools/1.9
# Convert HISAT2 sam files to bam files and sort by aligned position
samtools sort -@ 40 M546.sam | samtools view -bS -F 0x04 - > M546.OnlyAligned.bam 
samtools sort -@ 40 M640.sam | samtools view -bS -F 0x04 - > M640.OnlyAligned.bam 
samtools sort -@ 40 M645.sam | samtools view -bS -F 0x04 - > M645.OnlyAligned.bam 
samtools sort -@ 40 M648.sam | samtools view -bS -F 0x04 - > M648.OnlyAligned.bam 
samtools sort -@ 40 M656.sam | samtools view -bS -F 0x04 - > M656.OnlyAligned.bam 
samtools sort -@ 40 M658.sam | samtools view -bS -F 0x04 - > M658.OnlyAligned.bam 
samtools sort -@ 40 M661.sam | samtools view -bS -F 0x04 - > M661.OnlyAligned.bam 
samtools sort -@ 40 M668.sam | samtools view -bS -F 0x04 - > M668.OnlyAligned.bam 
samtools sort -@ 40 M723.sam | samtools view -bS -F 0x04 - > M723.OnlyAligned.bam 
samtools sort -@ 40 M728.sam | samtools view -bS -F 0x04 - > M728.OnlyAligned.bam 
samtools sort -@ 40 M809.sam | samtools view -bS -F 0x04 - > M809.OnlyAligned.bam 
samtools sort -@ 40 M826.sam | samtools view -bS -F 0x04 - > M826.OnlyAligned.bam
samtools sort -@ 40 M801.sam | samtools view -bS -F 0x04 - > M801.OnlyAligned.bam

samtools merge -nurlf -b bamlist.fofn allBamMerged.bam
