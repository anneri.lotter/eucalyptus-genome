#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=60G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load Trimmomatic/0.39

java -jar $Trimmomatic PE -threads 8 -summary M546.txt -validatePairs M546_1.fq M546_2.fq \
	trim_M546_1.fq singles_M546_1.fq \
	trim_M546_2.fq singles_M546_2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M640.txt -validatePairs M640-1.fq M640-2.fq \
	trim_M640-1.fq singles_M640-1.fq \
	trim_M640-2.fq singles_M640-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30


java -jar $Trimmomatic PE -threads 8 -summary M645.txt M645-1.fq M645-2.fq \
	trim_M645-1.fq singles_M645-1.fq \
	trim_M645-2.fq singles_M645-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M648-1.txt -validatePairs M648-1.fq M648-2.fq \
	trim_M648-1.fq singles_M648-1.fq \
	trim_M648-2.fq singles_M648-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M656.txt -validatePairs M656-1.fq M656-2.fq \
	trim_M656-1.fq singles_M656-1.fq \
	trim_M656-2.fq singles_M656-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M658.txt -validatePairs M658-1.fq M658-2.fq \
	trim_M658-1.fq singles_M658-1.fq \
	trim_M658-2.fq singles_M658-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M661.txt -validatePairs M661-1.fq M661-2.fq\
	trim_M661-1.fq singles_M661-1.fq \
	trim_M661-2.fq singles_M661-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M668.txt -validatePairs M668-1.fq M668-2.fq \
	trim_M668-1.fq singles_M668-1.fq \
	trim_M668-2.fq singles_M668-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M723.txt -validatePairs M723-1.fq M723-2.fq \
	trim_M723-1.fq singles_M723-1.fq \
	trim_M723-2.fq singles_M723-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M728.txt -validatePairs M728-1.fq M728-2.fq \
	trim_M728-1.fq singles_M728-1.fq \
	trim_M728-2.fq singles_M728-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M809.txt -validatePairs M809-1.fq M809-2.fq \
	trim_M809-1.fq singles_M809-1.fq \
	trim_M809-2.fq singles_M809-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M826.txt -validatePairs M826-1.fq M826-2.fq \
	trim_M826-1.fq singles_M826-1.fq \
	trim_M826-2.fq singles_M826-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary M801.txt -validatePairs M801-1.fq M801-2.fq\
	trim_M801-1.fq singles_M801-1.fq \
	trim_M801-2.fq singles_M801-2.fq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
	LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:30

