#!/bin/bash
#SBATCH --job-name=busco_multi
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

hostname
date

##########################################################
##              BUSCO                                   ##      
##########################################################

module load busco/4.0.2


busco -i final_o_prt/genes.fasta.faa \
        -o busco_final_prt \
        -c 8 \
        -l /isg/shared/databases/BUSCO/odb10_old/embryophyta_odb10 -m prot


