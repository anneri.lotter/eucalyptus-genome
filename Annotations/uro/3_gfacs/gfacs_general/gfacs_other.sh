#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=40G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date


module load perl/5.32.1

genome="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/uro/hisat2/uro_final_masked.fasta"
alignment="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/uro/hisat2/07_braker/braker/Sp_6/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d mono_o_prt ]; then
        mkdir mono_o_prt 
fi
if [ ! -d multi_o_prt ]; then
        mkdir multi_o_prt
fi

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-multiexonics \
	--rem-all-incompletes \
	--rem-genes-without-start-codon \
	--rem-genes-without-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O mono_o_prt \
	"$alignment" 

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-monoexonics \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--min-exon-size 6 \
	--get-protein-fasta \
	--fasta "$genome" \
	-O multi_o_prt \
	"$alignment"

date

cd mono_o_prt
if [ ! -d interproscan ]; then
        mkdir interproscan
fi
cd interproscan

echo "#!/bin/bash
#SBATCH --job-name=interproscan_pfam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o interproscan_pfam_%j.out
#SBATCH -e interproscan_pfam_%j.err

module load interproscan

sed 's/*$//g' ../genes.fasta.faa > genes.fasta.faa

interproscan.sh -appl Pfam -i genes.fasta.faa" > interproscan_pfam.sh

sbatch --wait interproscan_pfam.sh

cd ../

sed 's/\s.*$//' interproscan/genes.fasta.faa.tsv | uniq > pfam_ids.txt

python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList pfam_ids.txt$

cd ../multi_o_prt
# remove lines with 5_INC+3_INC here
grep '5_INC+3_INC' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > fullinc_ids.txt
grep 'gene' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > gene_table_ids.txt
grep -vxFf fullinc_ids.txt gene_table_ids.txt > comp_inc_ids.txt
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList comp_inc_ids.txt --idPath . --out comp_inc_gene_table.txt

if [ ! -d start_o ]; then
	mkdir start_o
fi

if [ ! -d stop_o ]; then
	mkdir stop_o
fi

alignment="comp_inc_gene_table.txt"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-start-codon \
--fasta "$genome" \
-O start_o \
"$alignment"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-stop-codon \
--fasta "$genome" \
-O stop_o \
"$alignment"

grep 'gene' start_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' | uniq > starts_ids.txt
grep 'gene' stop_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' |  uniq > stops_ids.txt
grep -vxFf starts_ids.txt comp_inc_ids.txt > missing_starts_ids.txt
grep -vxFf stops_ids.txt comp_inc_ids.txt > missing_stops_ids.txt
grep -xFf missing_starts_ids.txt missing_stops_ids.txt > missing_both_ids.txt
grep -vxFf missing_both_ids.txt comp_inc_ids.txt > completes_partials_ids.txt

python ../filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt

cd ../

cat mono_o_prt/pfam_gene_table.txt  multi_o_prt/gene_table.txt > mono_multi_gene_table.txt

if [ ! -d final_o_prt ]; then
        mkdir final_o_prt
fi

alignment="mono_multi_gene_table.txt"

perl "$script" \
	-f gFACs_gene_table \
	--no-processing \
	--statistics \
	--splice-table \
	--get-protein-fasta \
	--create-gff3 \
	--create-gtf \
	--fasta "$genome" \
	-O final_o_prt \
	"$alignment"


