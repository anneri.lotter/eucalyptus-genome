#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

hostname
date

##########################################################
##              BUSCO                                   ##      
##########################################################

module load busco/4.0.2

busco -i ./braker/Sp_2/augustus.hints.aa \
        -o braker_aa \
        -c 8 \
        -l /isg/shared/databases/BUSCO/odb10_old/embryophyta_odb10 -m prot


